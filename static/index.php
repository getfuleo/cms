<!-- Header / logo / menu -->
<?php include("header.php"); ?>
<section id="top-slider" class="slider">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <!-- single slide -->
                        <div class="carousel-item active">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-lg-5 text-center text-lg-left">
                                        <div class="txt">
                                            <h2 class="wavy">
                                                Zapisy na rok szkolny<br>
                                                2017/2018
                                            </h2>
                                            <p>
                                                Poznaj nasze przedszkole. Zapoznaj się z naszą ofertą.  
                                            </p>
                                            <a href="/offer.php" class="btn btn-outline-secondary">
                                                czytaj więcej
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 d-none d-lg-block">
                                        <div class="image">
                                            <img  src="assets/main/img/example-slide-1.png" alt="slajd">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- single slide -->
                        <div class="carousel-item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-lg-5 text-center text-lg-left">
                                        <div class="txt">
                                            <h2 class="wavy">
                                                Zapisy na rok szkolny<br>
                                                2017/2018
                                            </h2>
                                            <p>
                                                Poznaj nasze przedszkole. Zapoznaj się z naszą ofertą.  
                                            </p>
                                            <a href="/offer.php" class="btn btn-outline-secondary">
                                                czytaj więcej
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 d-none d-lg-block">
                                        <div class="image">
                                            <img src="assets/main/img/example-slide-1.png" alt="slajd">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="index-actual" class="actual section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>
                    Aktualności
                </h2>
                <p>
                    Zobacz co działo się w naszym przedszkolu
                </p>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img src="assets/main/img/example-gallery-4.jpg" alt="aktualność">
                    <h3>
                        Malujemy z uśmiechem
                    </h3>
                    <span>
                        czytaj więcej
                        <img src="assets/main/img/white-arrow.png" alt="strzałka">
                    </span>
                    <a href="#" title="Aktualność"></a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img src="assets/main/img/example-gallery-3.jpg" alt="aktualność">
                    <h3>
                        Malujemy z uśmiechem
                    </h3>
                    <span>
                        czytaj więcej
                        <img src="assets/main/img/white-arrow.png" alt="strzałka">
                    </span>
                    <a href="#" title="Aktualność"></a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img src="assets/main/img/example-gallery-2.jpg" alt="aktualność">
                    <h3>
                        Malujemy z uśmiechem
                    </h3>
                    <span>
                        czytaj więcej
                        <img src="assets/main/img/white-arrow.png" alt="strzałka">
                    </span>
                    <a href="#" title="Aktualność"></a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img src="assets/main/img/example-gallery-1.jpg" alt="aktualność">
                    <h3>
                        Malujemy z uśmiechem
                    </h3>
                    <span>
                        czytaj więcej
                        <img src="assets/main/img/white-arrow.png" alt="strzałka">
                    </span>
                    <a href="#" title="Aktualność"></a>
                </div>
            </div>
            <div class="col-12 text-center">
                <a href="#" class="btn btn-outline-black">czytaj więcej</a>
            </div> 
        </div>
    </div>
</section>

<?php include("module-quote.php"); ?>
<?php include("module-wall.php"); ?>

<section class="gallery">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>
                    Galeria
                </h2>
                <p>
                    Galeria naszych przedszkolaków
                </p>
            </div>
        </div>
        <!-- single item -->
        <div class="slick row">
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img src="assets/main/img/example-gallery-1.jpg" alt="galeria">
                    <a  title="Galeria" href="assets/main/img/example-gallery-1.jpg" data-lightbox="roadtrip"></a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img src="assets/main/img/example-gallery-1.jpg" alt="galeria">
                    <a  title="Galeria" href="assets/main/img/example-gallery-1.jpg" data-lightbox="roadtrip"></a>
                </div>
            </div>
            <!-- single item -->
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img  src="assets/main/img/example-gallery-2.jpg" alt="galeria">
                    <a  title="Galeria" href="assets/main/img/example-gallery-2.jpg" data-lightbox="roadtrip"></a>
                </div>
            </div>
            <!-- single item -->
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img  src="assets/main/img/example-gallery-3.jpg" alt="galeria">
                    <a  title="Galeria" href="assets/main/img/example-gallery-3.jpg" data-lightbox="roadtrip"></a>
                </div>
            </div>
            <!-- single item -->
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img  src="assets/main/img/example-gallery-4.jpg" alt="galeria">
                    <a  title="Galeria" href="assets/main/img/example-gallery-4.jpg" data-lightbox="roadtrip"></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <a href="#" class="btn btn-outline-secondary">czytaj więcej</a>
            </div> 
        </div>
    </div>
</div>
</section>
<section class="groups section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>
                    Grupy
                </h2>
                <p>
                    Poznaj naszych przedszkolaków
                </p>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="item">
                    <img class="img-fluid" src="assets/main/img/group-1.png" alt="grupa">
                    <h3>
                        Pszczółki
                    </h3>
                    <a href="#" title="Grupa"></a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="item">
                    <img class="img-fluid" src="assets/main/img/group-2.png" alt="grupa">
                    <h3>
                        Biedronki
                    </h3>
                    <a href="#"  title="Grupa"></a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="item">
                    <img class="img-fluid" src="assets/main/img/group-3.png" alt="grupa">
                    <h3>
                        Myszki
                    </h3>
                    <a href="#"  title="Grupa"></a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="item">
                    <img class="img-fluid" src="assets/main/img/group-4.png" alt="grupa">
                    <h3>
                        Kotki
                    </h3>
                    <a href="#"  title="Grupa"></a>
                </div>
            </div>
            <div class="col-12 text-center">
                <a href="#" class="btn btn-outline-black">czytaj więcej</a>
            </div> 
        </div>
    </div>
</section>
<?php include("module-slider.php"); ?>
<!-- Footer / contact / form /  menu-->
<?php
include("footer.php");
