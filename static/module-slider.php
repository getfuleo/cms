<section class="slider">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <!-- single slide -->
                        <div class="carousel-item active">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-7 d-none d-lg-block">
                                        <div class="image">
                                            <img src="assets/main/img/example-slide-2.png" alt="slajd">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-5 text-center text-lg-left">
                                        <div class="txt">
                                            <h2 class="wavy">
                                                Zapisy na rok szkolny<br>
                                                2017/2018
                                            </h2>
                                            <p>
                                                Poznaj nasze przedszkole. Zapoznaj się z naszą ofertą.  
                                            </p>
                                            <a href="#" class="btn btn-outline-secondary">
                                                czytaj więcej
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                              <!-- single slide -->
                        <div class="carousel-item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-7 d-none d-lg-block">
                                        <div class="image">
                                            <img src="assets/main/img/example-slide-2.png" alt="slajd">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-5 text-center text-lg-left">
                                        <div class="txt">
                                            <h2 class="wavy">
                                                Zapisy na rok szkolny<br>
                                                2017/2018
                                            </h2>
                                            <p>
                                                Poznaj nasze przedszkole. Zapoznaj się z naszą ofertą.  
                                            </p>
                                            <a href="#" class="btn btn-outline-secondary">
                                                czytaj więcej
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>