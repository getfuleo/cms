<!-- Header / logo / menu -->
<?php include("header.php"); ?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    Strefa rodzica 
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="login-page section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8 col-md-6 col-lg-4 text-center margin-auto">
                <div class="login-content">
                    <h3>
                        Strefa rodzica
                    </h3>
                    <p>
                        Dostęp do strefy rodzica możliwy jest po zalogowaniu się. Dane do logowania można uzyskać w sekretariacie przedszkola.
                    </p>
                    <form id="login-form">
 <input type="text" name="lname" placeholder="Wpisz login">
  <input type="text" name="lname" placeholder="Wpisz hasło">
  <input class="btn btn-success btn-submit" type="submit" value="Zaloguj się">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include("module-quote.php"); ?>
<?php include("module-wall.php"); ?>
<?php include("module-slider.php"); ?>
<!-- Footer / contact / form /  menu-->
<?php
include("footer.php");
