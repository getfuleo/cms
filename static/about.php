<!-- Header / logo / menu -->
<?php include("header.php"); ?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    O Nas 
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="contents section-white">
    <div class="container">
        <div class="row">
            <?php include("module-side-menu.php"); ?>
            <div class="about col-md-8">
                <div class="row">
                    <div class="col-12">
                        <p>
                            <b>
                                Misja
                            </b>
                            <br>
                            Przedszkole to drugi dom, w którym dziecko rozwija się i wychowuje, dlatego też nasze przedszkole wspiera rodziców w tak ważnym procesie jakim jest wychowanie.
                        </p>
                        <p>
                            „ Rzeczą najważniejszą jest wychowanie… jakie rzucisz ziarno takiego dochowasz się plonu.
                        </p>
                        <p>
                           Jeśli młodemu zaszczepisz zacne wychowanie będzie się ono rozwijać i kwitnąc przez całe życie i nie zniszczy go ani susza ani deszcz ”
                        </p>
                          <p>
                           Antyfona
                        </p>
                         <p>
                          Nasi wykwalifikowani pedagodzy czuwają nad rozwojem dziecka, jego indywidualizacją, jak również czynnie współpracują z rodzicami. W każdej chwili rodzice mogą zasięgnąć obiektywnej rady bądź opinii dotyczącej rozwoju swoich pociech. W naszym przedszkolu jest bezpiecznie, miło i przyjemnie. Dzieci są po prostu szczęśliwe a rodzice usatysfakcjonowani. Przedszkolaki z uśmiechem na twarzy wchodzą i wychodzą z naszej placówki. Nauczyciele zaś stwarzają odpowiedni klimat wychowawczy i dydaktyczny. Poprzez swoją pracę dydaktyczną nauczyciele odkrywają szczególne zdolności dziecka i dążą do ich rozwijania i doskonalenia. W pracy wykorzystują aktywizujące, czynnościowe metody sprzyjające rozwojowi umiejętności i zainteresowań dziecka m.in. Pedagogika Zabawy Klauza, edukacja matematyczna w/g koncepcji prof. E. Gruszczyk- Kolczyńskiej oraz edukacja teatralna.
                        </p>
                        <p>
                            Nasze przedszkole wychowuje dziecko aktywne, wrażliwe, samodzielne i radzące sobie z problemami. Przyzwyczaja do odpowiedzialności za własne bezpieczeństwo i bezpieczeństwo innych. Stwarza przyjemną domową atmosferę oraz zapewnia optymalne warunki do rozwijania osobowości dzieci. Wspiera w rozwijaniu zdolności i zainteresowań poprzez wyposażanie dzieci w wiedzę. Przekazując umiejętności niezbędne do powodzenia w dalszym życiu uczy samodzielności i niezależności. Wychowuje przedszkolaka tolerancyjnego, pełnego akceptacji i poszanowania dla odmienności innych ludzi. Podąża za indywidualnym tempem rozwoju dziecka respektując jego potrzeby, równocześnie otaczając je odpowiednią pomocą psychologiczną i logopedyczną.
                        </p>
                        <p>
                            Pedagodzy dążą do tego, aby Absolwent naszego przedszkola był twórczy, otwarty, asertywny, aktywny zdrowotnie i ekologicznie, posiadał umiejętność komunikowania się i potrafił współdziałać w zespole rówieśniczym.
                        </p>
                        <p>
Jednym zdaniem, aby był dobrze przygotowany do dalszej edukacji i wyzwań XXI wieku.
                        </p>
                        <p>
                            Przedszkole jest miejscem dobrej i bezpiecznej zabawy. Miejscem nauki i wypoczynku dzieci. Z tego 
                        </p>
                        <p>
                           „Zabawa jest nauką, nauka jest zabawą, 
                        </p>
                        <p>
                            im więcej zabawy, tym więcej nauki”
                        </p>
                        <p>
                            Przedszkole mieści się w dwóch nowoczesnych, wolnostojących budynkach o łącznej powierzchni ponad 1000 m2. Jest otoczone dużym, pełnym zieleni ogrodem.  W części ogrodu znajduje się wydzielona strefa dla zabaw na świeżym powietrzu. Znajduje się w niej drewniana altanka, ogromna piaskownica, która w słoneczne dni może być ocieniona parasolem, huśtawki, zjeżdżalnie oraz inne zabawki. Wszystkie zabawki ogrodowe posiadają wymagane certyfikaty.  W ogrodzie wydzielone zostały kąciki ekologiczne dla dzieci. Między innymi przedszkolaki obserwują i aktywnie uczestniczą w opiece nad sadem oraz warzywniakiem.  Od  najmłodszych lat uczą się jak pielęgnować zieleń w ogrodzie oraz jakie rośliny mogą w nim rosnąć. Mamy więc wrzosowisko, ogród skalny, sad, warzywniak oraz przepięknie na wiosnę kwitnącą część roślin cebulkowych: przebiśniegi, krokusy, żonkile i tulipany.
Budynek A, do którego uczęszczają dzieci najmłodsze (od 2 – 3 lat) jest złożony z trzech kondygnacji. Na parterze znajdują się trzy kolorowe, słoneczne sale z przylegającą do nich toaletą oraz jadalnia i kuchnia. Mamy również kącik wypoczynkowo – poczekalny dla rodziców, którzy oczekują na swoje dzieci. W tej sali robimy wernisaże prac naszych Wychowanków. Na piętrze również dysponujemy czterema salami, w pełni wyposażoną salą gimnastyczną, biblioteką wraz z kącikiem dla małego czytelnika.  Na tym poziomie również znajdują się dwie toalety. Szatnia dla dzieci jest na poziomie -1. 
Budynek B, w nim przebywają dzieci starsze (od 4 – 5 lat) składa się z dwóch kondygnacji. Na parterze jest przestronna szatnia, hol z kącikiem wypoczynkowo – poczekalnym dla rodziców, w którym również wystawiamy prace naszych Przedszkolaków oraz jadalnia i toaleta.
Na piętrze usytuowane zostały cztery przestronne sale. Sala gimnastyczna jest wyposażona w specjalistyczne podłogowe maty. W związku z tym mamy możliwość przeprowadzania takich zajęć jak np.: Aikido czy Gimnastyka dla Dzieci.
Sale w naszej placówce są jasne i przestronne. Zostały wyposażone w meble odpowiednio dobrane do wzrostu dzieci, co umożliwia im swobodne i bezpieczne poruszanie się. W każdej z nich znajdują się świadomie dobrane zabawki i pomoce edukacyjne. W tym zakresie współpracujemy z takimi firmami jak: Educarium, Novum, Nasza Szkoła, Nasze Bambino.  W części dydaktyczno – merytorycznej współpracujemy z takimi wydawnictwami jak Wydawnictwa Szkolne i Pedagogiczne, Wydawnictwo Forum – Poznań. Nasi Pedagodzy wspierają się specjalistycznymi publikacjami takimi jak: Głos Pedagogiczny, Bliżej Przedszkola oraz Doradca Dyrektora Przedszkola. 
                        </p>
                        <p>
                           Budynki spełniają wymogi UE, prawa budowlanego, powiatowej stacji sanitarno-epidemiologicznej, przepisów ochrony przeciwpożarowej i BHP. 
                        </p>
                        <p>
                          Budynek i otoczenie przedszkola są ogrodzone, zamknięte bramkami z videofonem oraz wejściem na kod.  Budynek ma całodobowy monitoring firmy Solid Security.
Ze względów bezpieczeństwa, dzieci mogą być odbierane z przedszkola wyłącznie przez rodziców, opiekunów lub inne osoby dorosłe pisemnie przez nich upoważnione. Do dyspozycji rodziców jest również wydzielony, odrębny parking.  
                        </p>
                    </div>
                </div>     
            </div>
        </div>
    </div>
</section>
<?php include("module-quote.php"); ?>
<?php include("module-wall.php"); ?>
<?php include("module-slider.php"); ?>
<!-- Footer / contact / form /  menu-->
<?php
include("footer.php");
