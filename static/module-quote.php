<aside class="quote">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3>
                    Dziecko może nauczyć dorosłych trzech rzeczy: cieszyć się bez powodu, 
                    być ciągle czymś zajętym i domagać się ze wszystkich sił tego, czego pragnie.  
                </h3>
                <p class="quote-author">
                    ~Paulo Coelho
                </p>
            </div>
        </div>
    </div>
</aside>