<!-- Header / logo / menu -->
<?php include("header.php"); ?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    O Nas 
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="contents section-white">
    <div class="container">
        <div class="row">
            <?php include("module-side-menu.php"); ?>
            <div class="about col-md-8">
                <div class="row">
                    <div class="col-12">
                        <h3>
                            Kontakt
                        </h3>
                    </div>
                    <div class="col-lg-6 col-xl-5">
                        <p>
                            <b>
                                NIEPUBLICZNE PRZEDSZKOLE CYPISEK
                            </b>
                            <br>
                            UL. OSTRÓDZKA
                            <br>
                            03-289 Warszawa
                        </p>
                        <p>
                            <b>
                                ADRESY E-MAIL:
                            </b>
                            <br>
                            <b>
                                SEKRETARIAT:
                            </b>
                            <br>
                            <a href="mailto:sekretariat@cypisek.edu.pl" title="napisz">
                                sekretariat@cypisek.edu.pl
                            </a>
                            <br>
                            <b>
                                DYREKTOR:
                            </b>
                            <br>
                            <a href="mailto:dyrektor@cypisek.edu.pl" title="napisz">
                                dyrektor@cypisek.edu.pl
                            </a>
                        </p>
                    </div>
                    <div class="col-lg-6 col-xl-3">
                        <p>
                            <b>
                                GODZINY OTWARCIA:
                            </b>
                            <br>
                            Poniedziałek-Piątek
                            <br>
                            6:30 - 18:00
                        </p>
                    </div>
                    <div class="col-lg-6 col-xl-3">
                        <p>
                            <b>
                                TELEFONY KONTAKTOWE:
                            </b>
                            <br>
                            Poniedziałek-Piątek
                            <br>
                            6:30 - 18:00
                        </p>
                    </div>
                    <div class="col-lg-6 col-xl-1 text-left text-xl-right">
                        <a href="#" title="przejdź do facebooka">
                            <img class="img-fluid" src="assets/main/img/fb.png" alt="ikony">
                        </a>
                    </div>
                    <div class="col-12">
                        <div id="map" class="maps">

                        </div>

                    </div>
                </div>     
            </div>
        </div>
    </div>
</section>
<?php include("module-quote.php"); ?>
<?php include("module-wall.php"); ?>
<?php include("module-slider.php"); ?>
<!-- Footer / contact / form /  menu-->
<?php
include("footer.php");
