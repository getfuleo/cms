<!-- Header / logo / menu -->
<?php include("header.php"); ?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    O Nas 
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="contents section-white">
    <div class="container">
        <div class="row">
            <?php include("module-side-menu.php"); ?>
            <div class="people col-md-8">
                <div class="row">
                    <div class="col-12">
                        <h3>
                            Kadra
                        </h3>
                        <!-- single- person -->
                        <div class="single">
                            <div class="row">
                            <div class="col-12  col-lg-3 ">
                                <div class="item">
                                    <img  src="assets/main/img/people-1.jpg" alt="kadra">
                                </div>
                            </div>
                            <div class="col-12 col-lg-9">
                                <h4>
                                    Wiesława Dobrowolska
                                </h4>
                                <span class="proffesion">
                                    Właściciel
                                </span>
                                <p>
            Wycieczka do Fabryki Elfów na Stadionie Narodowym 
(Grupa Misie i Biedronki) wyjazd godz. 12.30
                                </p>
                            </div>
                        </div>
                            </div>
                        <!-- single- person -->
                             <div class="single">
                            <div class="row">
                            <div class="col-12  col-lg-3 ">
                                <div class="item">
                                    <img  src="assets/main/img/people-2.jpg" alt="kadra">
                                </div>
                            </div>
                            <div class="col-12 col-lg-9">
                                <h4>
                                    Wiesława Dobrowolska
                                </h4>
                                <span class="proffesion">
                                    Właściciel
                                </span>
                                <span class="group-list group-1">
                                    Grupa: Kotki
                                </span>
                                <p>
                                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at 
nibh massa. Aliquam ipsum enim, suscipit non gravida dapibus, pretium 
malesuada erat. 
                                </p>
                            </div>
                        </div>
                            </div>
                                 <!-- single- person -->
                             <div class="single">
                            <div class="row">
                            <div class="col-12  col-lg-3">
                                <div class="item">
                                    <img  src="assets/main/img/people-3.jpg" alt="kadra">
                                </div>
                            </div>
                            <div class="col-12 col-lg-9">
                                <h4>
                                    Wiesława Dobrowolska
                                </h4>
                                <span class="proffesion">
                                    Właściciel
                                </span>
                                <span class="group-list group-1">
                                    Grupa: Kotki
                                </span>
                                <p>
                                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at 
nibh massa. Aliquam ipsum enim, suscipit non gravida dapibus, pretium 
malesuada erat. 
                                </p>
                            </div>
                        </div>
                            </div>
                                          <!-- single- person -->
                             <div class="single">
                            <div class="row">
                            <div class="col-12  col-lg-3 ">
                                <div class="item">
                                    <img  src="assets/main/img/people-4.jpg" alt="kadra">
                                </div>
                            </div>
                            <div class="col-12 col-lg-9">
                                <h4>
                                    Wiesława Dobrowolska
                                </h4>
                                <span class="proffesion">
                                    Właściciel
                                </span>
                                <span class="group-list group-2">
                                    Grupa: Misie
                                </span>
                                <p>
                                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at 
nibh massa. Aliquam ipsum enim, suscipit non gravida dapibus, pretium 
malesuada erat. 
                                </p>
                            </div>
                        </div>
                            </div>
                                                   <!-- single- person -->
                             <div class="single">
                            <div class="row">
                            <div class="col-12 col-lg-3 ">
                                <div class="item">
                                    <img  src="assets/main/img/people-5.jpg" alt="kadra">
                                </div>
                            </div>
                            <div class="col-12 col-lg-9">
                                <h4>
                                    Wiesława Dobrowolska
                                </h4>
                                <span class="proffesion">
                                    Właściciel
                                </span>
                                <span class="group-list group-2">
                                    Grupa: Misie
                                </span>
                                <p>
                                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at 
nibh massa. Aliquam ipsum enim, suscipit non gravida dapibus, pretium 
malesuada erat. 
                                </p>
                            </div>
                        </div>
                            </div>
                    </div>

                </div>     
            </div>
        </div>
    </div>
</section>
<?php include("module-slider.php"); ?>
<!-- Footer / contact / form /  menu-->
<?php
include("footer.php");
