<section class="wall section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="side">
                    <div class="text-center">
                        <h2>
                            Wydarzenia
                        </h2>
                        <p>
                            Wydarzenia, które odbędą się w naszym przedszkolu
                        </p>
                    </div>
                    <div class="item">
                        <div class="col-xs-12 no-padding">
                            <h3>
                                Spotkanie z policjantami
                            </h3>
                            <p>
                                W lipcu w naszym przedszkolu odbędzię się spotkanie z grupą 
                                policjantów, którzy nauczą dzieci zasad bezpieczeństwa
                                na drodzę oraz w miejscach publicznych.
                            </p>
                        </div>
                        <span>
                            10 lipiec 2017
                        </span>
                        <img src="assets/main/img/arrow.png" alt="galeria">
                        <a href="#" title="Wydarzenie"></a>
                    </div>
                    <div class="item">
                        <div class="col-xs-12 no-padding">
                            <h3>
                                Spotkanie z policjantami
                            </h3>
                            <p>
                                W lipcu w naszym przedszkolu odbędzię się spotkanie z grupą 
                                policjantów, którzy nauczą dzieci zasad bezpieczeństwa
                                na drodzę oraz w miejscach publicznych.
                            </p>
                        </div>
                        <span>
                            10 lipiec 2017
                        </span>
                        <img src="assets/main/img/arrow.png" alt="galeria">
                        <a href="#" title="Wydarzenie"></a>
                    </div>
                    <div class="item">
                        <div class="col-xs-12 no-padding">
                            <h3>
                                Spotkanie z policjantami
                            </h3>
                            <p>
                                W lipcu w naszym przedszkolu odbędzię się spotkanie z grupą 
                                policjantów, którzy nauczą dzieci zasad bezpieczeństwa
                                na drodzę oraz w miejscach publicznych.
                            </p>
                        </div>
                        <span>
                            10 lipiec 2017
                        </span>
                        <img src="assets/main/img/arrow.png" alt="galeria">
                        <a href="#" title="Wydarzenie"></a>
                    </div>
                    <div class="text-center text-lg-left">
                        <a href="#" class="btn btn-outline-success">więcej wydarzeń</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-6">

                <div class="side">
                    <div class="text-center">
                        <h2>
                            Ogłoszenia
                        </h2>
                        <p>
                            Wydarzenia, które odbędą się w naszym przedszkolu
                        </p>
                    </div>
                    <div class="item">
                        <div class="col-xs-12 no-padding">
                            <h3>
                                Spotkanie z policjantami
                            </h3>
                            <p>
                                W lipcu w naszym przedszkolu odbędzię się spotkanie z grupą 
                                policjantów, którzy nauczą dzieci zasad bezpieczeństwa
                                na drodzę oraz w miejscach publicznych.
                            </p>
                        </div>
                        <span>
                            10 lipiec 2017
                        </span>
                        <img src="assets/main/img/arrow.png" alt="galeria">
                        <a href="#" title="Ogłoszenie"></a>
                    </div>
                    <div class="item">
                        <div class="col-xs-12 no-padding">
                            <h3>
                                Spotkanie z policjantami
                            </h3>
                            <p>
                                W lipcu w naszym przedszkolu odbędzię się spotkanie z grupą 
                                policjantów, którzy nauczą dzieci zasad bezpieczeństwa
                                na drodzę oraz w miejscach publicznych.
                            </p>
                        </div>
                        <span>
                            10 lipiec 2017
                        </span>
                        <img src="assets/main/img/arrow.png" alt="galeria">
                        <a href="#" title="Ogłoszenie"></a>
                    </div>
                    <div class="item">
                        <div class="col-xs-12 no-padding">
                            <h3>
                                Spotkanie z policjantami
                            </h3>
                            <p>
                                W lipcu w naszym przedszkolu odbędzię się spotkanie z grupą 
                                policjantów, którzy nauczą dzieci zasad bezpieczeństwa
                                na drodzę oraz w miejscach publicznych.
                            </p>
                        </div>
                        <span>
                            10 lipiec 2017
                        </span>
                        <img src="assets/main/img/arrow.png" alt="galeria">
                        <a href="#" title="Ogłoszenie"></a>
                    </div>
                    <div class="text-center text-md-right">
                        <a href="#" class="btn btn-outline-success">więcej ogłoszeń</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>