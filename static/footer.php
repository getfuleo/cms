<footer class="footer section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6  col-lg-2 text-center text-lg-left">
                <div class="footer-combine">
                    <ul class="text-uppercase">
                        <li>
                            <a href="">
                                STRONA GŁÓWNA
                            </a>
                        </li>
                        <li>
                            <a href="offer.php">
                                OFERTA
                            </a>
                        </li>
                        <li>
                            <a href="events.php">
                                WYDARZENIA
                            </a>
                        </li>
                        <li>
                            <a href="events.php">
                                OGŁOSZENIA
                            </a>
                        </li>
                        <li>
                            <a href="about.php">
                                INFORMACJA DLA RODZICA
                            </a>
                        </li>
                        <li>
                            <a href="about.php">
                                GRUPY
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-2 text-center text-lg-left">
                <div class="footer-combine">
                    <ul class="text-uppercase">
                        <li class="active">
                            <a href="about.php">
                                O PRZEDSZKOLU
                            </a>
                        </li>
                        <li>
                            <a href="about.php">
                                O NAS
                            </a>
                        </li>
                        <li>
                            <a href="about.php">
                                INFORMACJA DLA RODZICA
                            </a>
                        </li>
                        <li>
                            <a href="people.php">
                                KADRY
                            </a>
                        </li>
                        <li>
                            <a href="contact.php">
                                KONTAKT
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6  col-lg-1 no-padding text-center  text-lg-left">
                <div class="footer-combine">
                    <img class="img-fluid" src="assets/main/img/logo.png" alt="logo">
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3 text-center text-lg-left">
                <div class="footer-combine">
                    <p>
                        <b>
                            NIEPUBLICZNE PRZEDSZKOLE CYPISEK
                        </b>
                        <br>
                        UL. OSTRÓDZKA 213K,
                        <br>
                        03-289 WARSZAWA
                    </p>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-2 no-padding text-center  text-lg-left">
                <div class="footer-combine">
                    <p>
                        <b>
                            GODZINY OTWARCIA:
                        </b>
                        <br>
                        PONIEDZIAŁEK-PIĄTEK
                        <br>
                        6:30 - 18:00
                        <br>
                        <b>
                            TELEFONY KONTAKTOWE:
                        </b>
                        <br>
                        <a href="tel:22 747 00 70" title="zadzwoń">
                            22 747 00 70
                        </a>
                        <br>
                        <a href="tel:22 747 00 70" title="zadzwoń">
                            0 606 202 215
                        </a>
                        <br>
                    </p>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-1 text-center text-lg-left">
                <div class="footer-combine">
                    <a href="#" title="przejdź do facebooka">
                        <img class="img-fluid" src="assets/main/img/fb.png" alt="ikony">
                    </a>
                </div>
            </div>
            <div class="col-12  text-center">
                <div class="footer-combine">
                    <div class="logo-bot">
                        <b>
                            projekt i wykonanie:
                        </b>
                        <img class="img-fluid" src="assets/main/img/innohouse.png" alt="logo">
                    </div>
                </div>
            </div>
        </div>
    </div>


</footer>
</main>
<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
<script src="assets/main/js/lightbox-plus-jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script src="assets/main/js/script.js"></script>
<script src="assets/main/js/slick.min.js"></script>
</body>
</html>