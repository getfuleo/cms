<!-- Header / logo / menu -->
<?php include("header.php"); ?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8">
                <h1 class="wavy">
                    Strefa rodzica 
                </h1>
            </div>
            <div class="col-12 col-sm-4 text-left text-sm-right">
                <a title="wyloguj się z panelu" href="login.php" class="btn btn-light">
                    <i class="icon i-off">
                    </i>
                    wyloguj się
                </a>
            </div>
        </div>
    </div>
</section>
<section class="parent-head section-white">
    <div class="container">
        <div class="row centered-row">
            <div class="col-md-2 d-none d-md-block">
                <div class="first">
                    <img src="assets/main/img/cat.png" alt="grupa">
                </div>
            </div>
            <div class="col-12 col-md-3 text-center text-lg-left ">
                <div class="second">
                    <h2>
                        Grupa Kotki
                    </h2>
                </div>
            </div>
            <div class="col-12 col-md-7 text-center text-md-left">
                <div class="third">
                    Przedszkole to drugi dom, w którym dziecko rozwija się i wychowuje, dlatego też nasze 
                    przedszkole wspiera rodziców w tak ważnym procesie jakim jest wychowanie.
                    Przedszkole to drugi dom, w którym dziecko rozwija się i wychowuje,
                </div>
            </div>
        </div>
    </div>
</section>
<section class="parent-page section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="main-infos">
                    <div class="infos infos-primary">
                        <div class="row">
                            <div class="col-12 col-lg-3 text-center">
                                <img class="img-fluid" src="assets/main/img/cat.png" alt="grupa">
                            </div>
                            <div class="col-12 col-lg-4 text-center no-padding">
                                <span>
                                    14
                                </span>
                                <p>
                                    <b>
                                        Ilość dzieci
                                    </b>
                                </p>
                            </div>
                            <div class="col-12 col-lg-5 text-center">
                                <span>
                                    2,5-3
                                </span>
                                <p>
                                    <b>
                                        Wiek dzieci
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="infos infos-secondary">
                        <div class="row">
                            <div class="col-12 col-lg-4 text-center text-lg-left">
                                <div class="item">
                                    <img src="assets/main/img/people-1.jpg" alt="grupa">
                                </div>
                            </div>
                            <div class="col-12 col-lg-8 no-padding text-center text-lg-left">
                                <h4>
                                    Ewa Mróz
                                </h4>
                                <p>
                                    <strong>
                                        Nauczyciel
                                    </strong>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="infos infos-secondary">
                        <div class="row">
                            <div class="col-12 col-lg-4 text-center text-lg-left">
                                <div class="item">
                                <img src="assets/main/img/people-2.jpg" alt="osoba">
                                </div>
                            </div>
                            <div class="col-12 col-lg-8 no-padding text-center text-lg-left">
                                <h4>
                                    Izabela Matyjanek
                                </h4>
                                <p>
                                    <strong>
                                        Asystent
                                    </strong>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="infos infos-primary">
                        <div class="row">
                            <div class="col-12 col-lg-5 text-center text-lg-left">
                                <p>
                                    <b>
                                        Sekretariat
                                    </b>
                                <p>
                            </div>
                            <div class="col-12 col-lg-7 text-center text-lg-left">
                                <b>
                                    <a href="tel:22 747 00 70">
                                        22 747 00 70
                                    </a>
                                </b>  
                                <i class="icon i-phone ">
                                </i>
                            </div>
                        </div>
                    </div>
                    <div class="infos infos-primary">
                        <div class="row">
                            <div class="col-12 text-center text-lg-left">
                                <p>
                                    <b>
                                        Budynek A
                                    </b>
                                <p>
                            </div>
                        </div>
                    </div>
                    <div class="infos infos-primary">
                        <div class="row">
                            <div class="col-12 text-center text-lg-left">
                                <p>
                                    <b>
                                        NIEPUBLICZNE PRZEDSZKOLE CYPISEK
                                    </b>
                                    <br>
                                    UL. OSTRÓDZKA 213K,
                                    <br>
                                    03-289 WARSZAWA
                                    <br>
                                    <br>
                                    <b>
                                        GODZINY OTWARCIA:
                                    </b>
                                    <br>
                                    PONIEDZIAŁEK-PIĄTEK
                                    <br>
                                    6:30 - 18:00
                                    <br>
                                    <br>
                                    <br>
                                    <b>
                                        TELEFONY KONTAKTOWE:
                                    </b>
                                    <br>
                                    <a href="tel:22 747 00 70" title="zadzwoń">
                                        22 747 00 70
                                    </a>
                                    <br>
                                    <a href="tel:22 747 00 70" title="zadzwoń">
                                        0 606 202 215
                                    </a>
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8">
                <div class="gallery-parent">
                    <div class="row">
                        <div class="col-12 ">
                            <h3>
                                Galeria
                            </h3>
                        </div>
                        <!-- single photo -->                    
                        <div class="col-12 col-sm-6 col-lg-3 ">
                            <div class="item">
                                <img src="assets/main/img/example-gallery-1.jpg" alt="galeria">
                                <a  href="assets/main/img/example-gallery-1.jpg" data-lightbox="roadtrip"></a>
                            </div>
                        </div>
                        <!-- single photo --> 
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="item">
                                <img  src="assets/main/img/example-gallery-1.jpg" alt="galeria">
                                <a  href="assets/main/img/example-gallery-1.jpg" data-lightbox="roadtrip"></a>
                            </div>
                        </div>
                        <!-- single photo -->                    
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="item">
                                <img src="assets/main/img/example-gallery-1.jpg" alt="galeria">
                                <a href="assets/main/img/example-gallery-1.jpg" data-lightbox="roadtrip"></a>
                            </div>
                        </div>
                        <!-- single photo --> 
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="item">
                                <img  src="assets/main/img/example-gallery-1.jpg" alt="galeria">
                                <a  href="assets/main/img/example-gallery-1.jpg" data-lightbox="roadtrip"></a>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <a href="#" class="btn btn-outline-secondary">czytaj więcej</a>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include("module-quote.php"); ?>
<?php include("module-wall.php"); ?>
<?php include("module-slider.php"); ?>
<!-- Footer / contact / form /  menu-->
<?php
include("footer.php");
