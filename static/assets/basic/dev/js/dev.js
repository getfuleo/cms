$(document).ready(function () {

//jQuery

// JavaScript


$('.slick').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  
    responsive: [{

      breakpoint: 992,
      settings: {
        slidesToShow: 3
      }

    }, {

      breakpoint: 767,
      settings: {
        slidesToShow: 2
      }
      
       }, {

      breakpoint: 576,
      settings: {
        slidesToShow: 1
      }

    }, {

      breakpoint: 300,
      settings: "unslick" // destroys slick

    }]
});


$(document).scroll(function() { 
   if($(window).scrollTop() > $(".header").height() + $("#top-slider").height() + $("#index-actual").height() + 150) {
    $(".wall").css("background-position", "0px");
   }
});


});



/**
 *
 * DOCUMENT RESIZE
 *
 */






/*================================
 =            FUNCTION            =
 ================================*/




/*=====  End of FUNCTION  ======*/