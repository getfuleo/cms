<!-- Header / logo / menu -->
<?php include("header.php"); ?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    Oferta 
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="offer section-white">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>
                    <b>
                        Rok szkolny 2016/2017   
                    </b>
                    <br>
                    W ramach czesnego przedszkole zapewnia:
                </p>
                <p>
                    • całodzienną opiekę pedagogiczną (w godzinach: 6.30 – 18.00)
                </p>
            </div>
            <div class="col-12">
                <div class="table">
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        Zajęcia dydaktyczne wg tygodniowego rozkładu zajęć <br>
                                        dostosowanego do grupy wiekowej i zgodnego z programem nauczania MEN
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    j. angielski
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    4 x w tygodniu, zajęcia z native speakerem
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    gimnastyka korekcyjna
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    zajęcia odbywają się 1 x w tygodniu
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    rytmika/umuzykalnienie
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    zajęcia odbywają się 1 x w tygodniu
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    zajęcia grupowe z logopedą
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    zajęcia odbywają się 1 x w tygodniu
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    przesiewowe badania logopedyczne
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    2 razy w roku szkolnym
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    grupowe zajęcia z psychologiem
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    2 razy w miesiącu
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    zajęcia ruchowo-sportowe
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    1 x w tygodniu po 45min
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    zajęcia kulinarne
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    1 x w miesiącu
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    koło plastyczne
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    2 x w miesiącu
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row (empty) -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <span>-</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table">
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        Dodatkowo:
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        koncerty muzyczne w wykonaniu Filharmonii Narodowej w Warszawie
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        przedstawienia teatralne w przedszkolu
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        organizację Dnia Dziecka
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        organizację Pikniku Rodzinnego
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        prezenty gwiazdkowe z okazji 6 grudnia i Świąt Bożego Narodzenia
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        wycieczki poza obręb przedszkola (za dopłatą rodziców)
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        zwiedzanie obiektów kulturalnych i użyteczności społecznej: muzea, biblioteka, poczta, szkoła, parki;
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        wyjazdy do teatru i na wydarzenia kulturalne
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        zakup podręczników dla dzieci z grup 4-5 latków do pracy indywidualnej (płatne przez rodziców)
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row (empty) -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <span>-</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table">
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        Zajęcia organizowane przez przedszkole <br>
                                        w godzinach popołudniowych płatne przez rodziców
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    basen
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    zajęcia odbywają się 1 x w tygodniu
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    warsztaty taneczne ( taniec towarzyski)
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    zajęcia odbywają się 1 x w tygodniu
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    ceramika
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    zajęcia odbywają się 1 x w tygodniu
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    piła nożna
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    zajęcia odbywają się 1 x w tygodniu
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-8">
                                <p>
                                    robotyka
                                </p>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                                <p>
                                    2 razy w roku szkolnym
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row (empty) -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <span>-</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table">
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    <b>
                                        Dodatkowe wydarzenia organizowane przez przedszkole Cypisek:
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    indywidualne zajęcia z logopedą
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    indywidualne zajęcia z psychologiem
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    „Noc w Cypisku"
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- single row -->
                    <div class="single">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    „Sylwester w Cypisku"
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include("module-quote.php"); ?>
<?php include("module-wall.php"); ?>
<?php include("module-slider.php"); ?>
<!-- Footer / contact / form /  menu-->
<?php
include("footer.php");
