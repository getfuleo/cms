<!-- Header / logo / menu -->
<?php include("header.php"); ?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    Wydarzenia
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="contents section-white">
    <div class="container">
        <div class="row">
<!--            <div class="side-menu col-md-4 d-none d-md-block">
                <p>
                    <b>
                        Wybierz rok
                    </b>
                </p>
                <ul>
                    <li class="active not-full">
                        <a href="events.php">
                            2017
                        </a>
                    </li>
                </ul>
                <p>
                    <b>
                        Wybierz miesiąc
                    </b>
                </p> 
                <ul class="">
                    <li class="active">
                        <a href="events.php">
                            01. Styczeń
                        </a>
                    </li>
                    <li >
                        <a href="events.php">
                            02. Luty
                        </a>
                    </li>
                    <li >
                        <a href="events.php">
                            03. Marzec
                        </a>
                    </li>
                    <li >
                        <a href="events.php">
                            04. Kwiecień
                        </a>
                    </li>
                    <li>
                        <a href="about.php">
                            05. Maj
                        </a>
                    </li>
                    <li>
                        <a href="events.php">
                            06. Czerwiec
                        </a>
                    </li>
                    <li>
                        <a href="events.php">
                            07. Lipiec
                        </a>
                    </li>
                    <li>
                        <a href="events.php">
                            08. Sierpień
                        </a>
                    </li>
                    <li>
                        <a href="events.php">
                            09. Wrzesień
                        </a>
                    </li>
                    <li class="disabled">
                        <a href="events.php">
                            10. Październik
                        </a>
                    </li>
                    <li class="disabled">
                        <a href="events.php">
                            11. Listopad
                        </a>
                    </li>
                    <li class="disabled">
                        <a href="events.php">
                            12. Grudzień
                        </a>
                    </li>
                </ul>
            </div>-->
            <div class="side-menu col-md-4 ">
                <div class="dropdown dropdowns-events">
                    <button class="btn btn-dropdown-events dropdown-toggle" type="button" data-toggle="dropdown">2017
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu first">
                        <li class="active">
                            <a href="events.php">
                                2016
                            </a>
                        </li>
                        <li>
                            <a href="events.php">
                                2017
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="dropdown dropdowns-events">
                    <button class="btn btn-dropdown-events dropdown-toggle" type="button" data-toggle="dropdown">01. Styczeń
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li class="active">
                            <a href="events.php">
                                01. Styczeń
                            </a>
                        </li>
                        <li>
                            <a href="events.php">
                                02. Luty
                            </a>
                        </li>
                        <li>
                            <a href="events.php">
                                03. Marzec
                            </a></li>
                        <li >
                            <a href="events.php">
                                04. Kwiecień
                            </a>
                        </li>
                        <li>
                            <a href="about.php">
                                05. Maj
                            </a>
                        </li>
                        <li>
                            <a href="events.php">
                                06. Czerwiec
                            </a>
                        </li>
                        <li>
                            <a href="events.php">
                                07. Lipiec
                            </a>
                        </li>
                        <li>
                            <a href="events.php">
                                08. Sierpień
                            </a>
                        </li>
                        <li>
                            <a href="events.php">
                                09. Wrzesień
                            </a>
                        </li>
                        <li class="disabled">
                            <a href="events.php">
                                10. Październik
                            </a>
                        </li>
                        <li class="disabled">
                            <a href="events.php">
                                11. Listopad
                            </a>
                        </li>
                        <li class="disabled">
                            <a href="events.php">
                                12. Grudzień
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="events col-md-8">
                <div class="row">
                    <div class="col-12">
                        <h3>
                            <span>Wydarzenia w przedszkolu Cypisek w miesiącu:</span> Sierpień 2017
                        </h3> 
                        <!-- single event -->
                        <div class="single">
                            <div class="row">
                                <div class="col-6 col-md-2 ">
                                    <span class="id">
                                        1
                                    </span>
                                </div>
                                <div class="col-6 d-md-none text-right">
                                    <span class="date"> 
                                        07.08
                                    </span>
                                </div>
                                <div class="col-12 col-md-8">
                                    <h4>
                                        Wycieczka do Fabryki Elfów 
                                    </h4>
                                    <p>
                                        Wycieczka do Fabryki Elfów na Stadionie Narodowym 
                                        (Grupa Misie i Biedronki) wyjazd godz. 12.30
                                    </p>
                                </div>
                                <div class=" col-12 col-md-2 d-none d-md-block">
                                    <span class="date"> 
                                        07.08
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!-- single event -->
                        <div class="single">
                            <div class="row">
                                <div class="col-6 col-md-2 ">
                                    <span class="id">
                                        1
                                    </span>
                                </div>
                                <div class="col-6 d-md-none text-right">
                                    <span class="date"> 
                                        07.08
                                    </span>
                                </div>
                                <div class="col-12 col-md-8">
                                    <h4>
                                        Wycieczka do Fabryki Elfów 
                                    </h4>
                                    <p>
                                        Wycieczka do Fabryki Elfów na Stadionie Narodowym 
                                        (Grupa Misie i Biedronki) wyjazd godz. 12.30
                                    </p>
                                </div>
                                <div class=" col-12 col-md-2 d-none d-md-block">
                                    <span class="date"> 
                                        07.08
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!-- single event -->
                        <div class="single">
                            <div class="row">
                                <div class="col-6 col-md-2 ">
                                    <span class="id">
                                        1
                                    </span>
                                </div>
                                <div class="col-6 d-md-none text-right">
                                    <span class="date"> 
                                        07.08
                                    </span>
                                </div>
                                <div class="col-12 col-md-8">
                                    <h4>
                                        Wycieczka do Fabryki Elfów 
                                    </h4>
                                    <p>
                                        Wycieczka do Fabryki Elfów na Stadionie Narodowym 
                                        (Grupa Misie i Biedronki) wyjazd godz. 12.30
                                    </p>
                                </div>
                                <div class=" col-12 col-md-2 d-none d-md-block">
                                    <span class="date"> 
                                        07.08
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>     

        </div>
    </div>
</section>
<?php include("module-quote.php"); ?>
<?php include("module-wall.php"); ?>
<?php include("module-slider.php"); ?>
<!-- Footer / contact / form /  menu-->
<?php
include("footer.php");
