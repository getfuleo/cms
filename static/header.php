<!DOCTYPE html>
<html lang="pl">
    <head prefix="og: http://ogp.me/ns#">
        <meta charset="UTF-8">
        <title>Cypisek</title>
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="robots" content="index,fallow">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">

        <meta property="og:locale" content="pl_PL">
        <meta property="og:url" content="http://">
        <meta property="og:title" content="">
        <meta property="og:site_name" content="">
        <meta property="og:description" content="">
        <meta property="og:type" content="website">
        <meta property="og:image" content="http://">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="201">
        <meta property="og:image:height" content="201">

        <link rel="icon" type="image/png" href="http://">

        <link rel="stylesheet" href="assets/main/css/library.css">

        <link rel="stylesheet" href="assets/main/css/style.css">
        <link rel="stylesheet" href="assets/main/css/slick.css">
        <link rel="stylesheet" href="assets/main/css/slick-theme.css">
        <link rel="stylesheet" href="assets/main/css/lightbox.min.css" type="text/css">

        <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">
        <!--[if lt IE 9]>
            <script src="assets/main/js/modules/hack.js"></script>
        <![endif]-->
    </head>
    <body>
        <main>
            <header class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-right">
                            <a  href="parent-page.php" class="btn btn-danger">
                                Strefa rodzica
                            </a>
                            <a class="btn btn-phone" href="tel:22 747 00 70" title="zadzwoń">
                                <img class="img-fluid" src="/img/big-phone.png" alt="logo">
                            </a>
                        </div>
                        <div class="col-12 text-right">
                            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                                <a class="navbar-brand " href="/" titel="przejdź do strony głównej">
                                    <img class="img-fluid" src="/img/big-logo.png" alt="logo">
                                </a>
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                                    <ul class="navbar-nav">
                                        <li class="nav-item active">
                                            <a class="nav-link  d-none d-lg-block" href="/cypisek/">
                                                <i class="icon i-home"></i>
                                            </a>
                                            <a class="nav-link d-lg-none" href="/cypisek/">
                                                strona główna
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="offer.php">oferta</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="events.php">wydarzenia</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="events.php">ogłoszenia</a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                informacje dla rodzica
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                <a class="dropdown-item" href="about.php">Adaptacja</a>
                                                <a class="dropdown-item" href="about.php">Dni wolne</a>
                                            </div>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                o nas
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                <a class="dropdown-item" href="about.php">O przedszkolu</a>
                                                <a class="dropdown-item" href="about.php">Misja przedszkola</a>
                                                <a class="dropdown-item" href="about.php">Galeria</a>
                                                <a class="dropdown-item" href="people.php">Kadra</a>
                                                <a class="dropdown-item" href="contact.php">Kontakt</a>
                                            </div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="contact.php">kontakt</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>