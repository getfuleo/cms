<?php

namespace common\widgets;

use Yii;
use common\models\modules\Modules;
use common\models\pages\Pages;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Widget Block
 */
class Block extends \yii\bootstrap\Widget {

    public $data;
    public $slug;
    public $model = false;
    public $models = false;
    public $searchModel = false;
    public $dataProvider = false;
    public $type = false;

    public function init() {
        parent::init();
    }

    public function run() {






        $module = Modules::findOne($this->data->module_id);

        if ($module->categories) {

            $this->model = $module->namespace::findOne($this->data->data_id);
            $this->searchModel = new $module->namespace_child();
            $this->dataProvider = $this->searchModel->search(Yii::$app->request->queryParams);
            $this->dataProvider->query->where(["categories_id" => $this->data->data_id]);
            $this->dataProvider->setSort([
                    'defaultOrder' => [
                        'id' =>  $this->data->order ?? SORT_DESC
                    ]
                ]);
            $this->dataProvider->pagination = array(
                'pagesize' => $this->data->limit_data,
                'route' => Pages::findOne($this->data->site_id)->slug
            );
 

            $render = '@frontend/views/' . $module->folder . '/block/' . $this->data->layout;
        } else {

            // if is data id - search one
            if ($this->data->data_id) {
                $this->model = $module->namespace::findOne($this->data->data_id);
            } else {
                // Search all records with limit
                $this->searchModel = new $module->namespace();
                $this->dataProvider = $this->searchModel->search(Yii::$app->request->queryParams);
                $this->dataProvider->setSort([
                    'defaultOrder' => [
                        'id' =>  $this->data->order ?? SORT_DESC
                    ]
                ]);
                $this->dataProvider->pagination = array(
                    'pagesize' => $this->data->limit_data
                );
            }


            // $this->searchModel = new $module->namespace();
            //$this->dataProvider = $this->searchModel->search(Yii::$app->request->queryParams);
            //$this->model = $this->dataProvider->query->where([$this->searchModel::tableName() . ".id" => $this->data->data_id]);
            $render = '@frontend/views/' . $module->folder . '/block/' . $this->data->layout;
        }



        return $this->render($render, [
                    'searchModel' => $this->searchModel,
                    'dataProvider' => $this->dataProvider,
                    'model' => $this->model,
                    'models' => $this->models,
                    'slug' => $this->slug,
                    'type' => $this->type
        ]);
    }

    public function menu() {
        
    }

}
