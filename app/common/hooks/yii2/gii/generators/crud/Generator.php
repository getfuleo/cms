<?php
namespace common\hooks\yii2\gii\generators\crud;

use Yii;
use common\modules\modules\models\Module;
use common\modules\pages\models\PagesAdmin;

class Generator extends \yii\gii\generators\crud\Generator
{
    
    public function save($files, $answers, &$results) {
        $result = parent::save($files, $answers, $results);
        if($result){
            $aRawControllerClass = explode("\\", $this->controllerClass);
            $sModuleName = str_replace("Controller", "", end($aRawControllerClass));
            $sModuleControllerClass = str_replace("Controller", "", str_replace("controllers\\", "", $this->controllerClass));
            
            $oModule = new Module();
            $oModule->created_at = date("Y-m-d H:i:s");
            $oModule->name = $sModuleName;
            $oModule->display_name = $sModuleName;
            $oModule->admin = $sModuleControllerClass;
            $oModule->model = $this->modelClass;
            $oModule->is_active = 1;
            $oModule->save();
            
            $oPagesAdmin = new PagesAdmin();
            $oPagesAdmin->parent_id = 70;
            $oPagesAdmin->module_id = $oModule->id;
            $oPagesAdmin->name = $sModuleName;
            $oPagesAdmin->slug = $this->prepareURL($sModuleName);
            $oPagesAdmin->url = $this->prepareURL($sModuleName);
            $oPagesAdmin->is_active = 1;
            $oPagesAdmin->created_at = date("Y-m-d H:i:s");
            $oPagesAdmin->sort = 0;
            $oPagesAdmin->save();
            
            $auth = Yii::$app->authManager;
            
            $permission = $auth->getPermission(strtolower($sModuleName)."_view");
            if(is_null($permission)){
                $permission = $auth->createPermission(strtolower($sModuleName)."_view");
                $permission->description = "Uprawnienia modułu: {$sModuleName}";
                $auth->add($permission);
            }
            
            $role = $auth->getRole("admin");
            if(!Yii::$app->user->can(strtolower($sModuleName)."_view")){
                $auth->addChild($role, $permission);
            }
            
        }
        
        return $result;
    }
    
    protected function prepareURL($sText){
        $aReplacePL = array( 'ą' => 'a', 'ę' => 'e', 'ś' => 's', 'ć' => 'c', 'ó' => 'o', 'ń' => 'n', 'ż' => 'z', 'ź' => 'z', 'ł' => 'l', 'Ą' => 'A', 'Ę' => 'E', 'Ś' => 'S', 'Ć' => 'C', 'Ó' => 'O', 'Ń' => 'N', 'Ż' => 'Z', 'Ź' => 'Z', 'Ł' => 'L');
        $sText = str_replace(array_keys($aReplacePL), array_values($aReplacePL), $sText);
        $sText = str_replace("?","", $sText);
        $sText = str_replace("„","", $sText);
        $sText = str_replace("”","", $sText);
        $sText = str_replace(' ', '-', strtolower($sText));
        $sText = preg_replace('/[\-]+/', '-', $sText);
        $sText = preg_replace('/[^A-Za-z0-9 -]/u', '', $sText);
        
        return trim($sText, '-');
    }
    
}