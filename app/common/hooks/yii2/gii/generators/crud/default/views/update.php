<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

$urlParams = $generator->generateUrlParams();

$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = <?= $generator->generateString('Update {modelClass}: ', ['modelClass' => Inflector::camel2words(StringHelper::basename($generator->modelClass))]) ?> . $model-><?= $generator->getNameAttribute() ?>;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-update">

    <p>
        <?= "<?= " ?>Html::a('< Wróć  do listy', ['index'], ['class' => 'btn btn-default']) ?>
    </p>

     <section class="panel full" >
        <header>
            <img src="/__cms/images/login/logo-gray.png" alt="logo"/> <?= "<?= " ?>Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?= "<?php " ?>$form = ActiveForm::begin(); ?>

            <?php foreach ($generator->getColumnNames() as $attribute) {
                if (in_array($attribute, $safeAttributes)) {
                    echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
                }
            } ?>
           <div class="form-group">
               <?= "<?= " ?>Html::submitButton($model->isNewRecord ? <?= $generator->generateString('Create') ?> : <?= $generator->generateString('Update') ?>, ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
           </div>

           <?= "<?php " ?>ActiveForm::end(); ?>
        </div>
    </section>

</div>
