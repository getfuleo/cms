<?php
namespace common\hooks\yii2\base;

use Yii;

class Module extends \yii\base\Module
{
    private $allowedActions = ['view','edit','create','update'];
    
    public function beforeAction($action) {
        if ( parent::beforeAction($action) ) {
            $sActionType = strtolower(str_replace("action", "", $action->actionMethod));

            if($sActionType == "index") $sActionType = "view";
            if(in_array($sActionType, $this->allowedActions)){
                if(!Yii::$app->user->can("{$this->module->controller->id}_{$sActionType}")){
                    //var_dump("{$this->module->controller->id}_{$sActionType}");
                    //Yii::$app->getResponse()->redirect(Yii::$app->homeUrl)->send();
                    //return false;
                }
            }
            return true;
        } else {
            return false;
        }
        
        return parent::beforeAction($action);
    }
    
}