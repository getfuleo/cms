<?php
namespace common\hooks\yii2\widgets;
class Breadcrumbs extends \yii\widgets\Breadcrumbs
{
    public $tag = 'ol';
    public $options = ['class' => 'breadcrumb'];
    public $itemTemplate = "<li class='breadcrumb-item'>{link}</li>\n";
    public $activeItemTemplate = "<li class=\"breadcrumb-item active\">{link}</li>\n";
}