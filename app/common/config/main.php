<?php
return [
    'language' => 'pl-PL',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
          'i18n' => [
        'translations' => [
             '*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@common/messages', // if advanced application, set @frontend/messages
                'sourceLanguage' => 'pl',
                'fileMap' => [
                    //'main' => 'main.php',
                ],
            ],
//            'frontend*' => [
//                'class' => 'yii\i18n\PhpMessageSource',
//                'basePath' => '@common/messages',
//            ],
//            'backend*' => [
//                'class' => 'yii\i18n\PhpMessageSource',
//                'basePath' => '@common/messages',
//            ],
        ],
    ],
    ],
];
