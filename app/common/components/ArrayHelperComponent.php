<?php


namespace common\components;

class ArrayHelperComponent {
    
    
    /*
     * Array Group Helper
     */

    public function arrayGroup(array $data, $by_column) {
        $result = [];

        foreach ($data as $item) {
            $column = $item[$by_column];
            unset($item[$by_column]);
            if (isset($result[$column])) {
                $result[$column][] = $item;
            } else {
                $result[$column] = array($item);
            }
        }

        return $result;
    }

}
