<?php

namespace common\models\classifieds;

use Yii;
use yii\behaviors\SluggableBehavior;
/**
 * This is the model class for table "modules_classifieds".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $description_short
 * @property string $date_create
 * @property string $date_update
 * @property integer $user_create
 * @property integer $user_update
 * @property integer $active
 * @property integer $categories_id
 * @property string $date
 * @property integer $slug
 *
 * @property ModulesClassifiedsCategories $categories
 */
class Classifieds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modules_classifieds';
    }
    public function behaviors() {
        return [
                [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'description_short'], 'string'],
            [['date_create', 'date_update', 'date','date_at', 'date_to' ,'slug'], 'safe'],
            [['user_create', 'user_update', 'active', 'categories_id','date_show'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClassifiedsCategories::className(), 'targetAttribute' => ['categories_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('classifieds', 'ID'),
            'title' => Yii::t('classifieds', 'Title'),
            'description' => Yii::t('classifieds', 'Description'),
            'description_short' => Yii::t('classifieds', 'Description Short'),
            'date_create' => Yii::t('classifieds', 'Date Create'),
            'date_update' => Yii::t('classifieds', 'Date Update'),
            'user_create' => Yii::t('classifieds', 'User Create'),
            'user_update' => Yii::t('classifieds', 'User Update'),
            'active' => Yii::t('classifieds', 'Active'),
            'categories_id' => Yii::t('classifieds', 'Categories ID'),
            'date' => Yii::t('classifieds', 'Date'),
            'slug' => Yii::t('classifieds', 'Slug'),
             'date_at' => Yii::t('classifieds', 'Date At'),
            'date_to' => Yii::t('classifieds', 'Date To'),
             'date_show' => Yii::t('classifieds', 'Date Show'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasOne(ClassifiedsCategories::className(), ['id' => 'categories_id']);
    }

    /**
     * @inheritdoc
     * @return ClassifiedsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClassifiedsQuery(get_called_class());
    }
}
