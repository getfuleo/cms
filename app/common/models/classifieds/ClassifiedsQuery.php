<?php

namespace common\models\classifieds;

/**
 * This is the ActiveQuery class for [[Classifieds]].
 *
 * @see Classifieds
 */
class ClassifiedsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Classifieds[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Classifieds|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
