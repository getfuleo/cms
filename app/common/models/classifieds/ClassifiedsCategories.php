<?php

namespace common\models\classifieds;

use Yii;

/**
 * This is the model class for table "modules_classifieds_categories".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $id_parent
 * @property string $description_short
 * @property string $date_create
 * @property string $date_update
 * @property integer $user_create
 * @property integer $user_update
 * @property integer $active
 *
 * @property ModulesClassifieds[] $modulesClassifieds
 */
class ClassifiedsCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modules_classifieds_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'description_short', 'date_create', 'date_update', 'user_create', 'user_update', 'active'], 'required'],
            [['description', 'description_short'], 'string'],
            [['id_parent', 'user_create', 'user_update', 'active'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('classifieds', 'ID'),
            'title' => Yii::t('classifieds', 'Title'),
            'description' => Yii::t('classifieds', 'Description'),
            'id_parent' => Yii::t('classifieds', 'Id Parent'),
            'description_short' => Yii::t('classifieds', 'Description Short'),
            'date_create' => Yii::t('classifieds', 'Date Create'),
            'date_update' => Yii::t('classifieds', 'Date Update'),
            'user_create' => Yii::t('classifieds', 'User Create'),
            'user_update' => Yii::t('classifieds', 'User Update'),
            'active' => Yii::t('classifieds', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModulesClassifieds()
    {
        return $this->hasMany(Classifieds::className(), ['categories_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ClassifiedsCategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClassifiedsCategoriesQuery(get_called_class());
    }
}
