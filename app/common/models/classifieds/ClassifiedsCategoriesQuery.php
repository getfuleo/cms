<?php

namespace common\models\classifieds;

/**
 * This is the ActiveQuery class for [[ClassifiedsCategories]].
 *
 * @see ClassifiedsCategories
 */
class ClassifiedsCategoriesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ClassifiedsCategories[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ClassifiedsCategories|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
