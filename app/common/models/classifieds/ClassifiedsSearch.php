<?php

namespace common\models\classifieds;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\classifieds\Classifieds;

/**
 * ClassifiedsSearch represents the model behind the search form about `common\models\classifieds\Classifieds`.
 */
class ClassifiedsSearch extends Classifieds
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_create', 'user_update', 'active', 'categories_id', 'slug'], 'integer'],
            [['title', 'description', 'description_short', 'date_create', 'date_update', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Classifieds::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
            'user_create' => $this->user_create,
            'user_update' => $this->user_update,
            'active' => $this->active,
            'categories_id' => $this->categories_id,
            'date' => $this->date,
            'slug' => $this->slug,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'description_short', $this->description_short]);

        return $dataProvider;
    }
}
