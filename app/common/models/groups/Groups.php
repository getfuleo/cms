<?php

namespace common\models\groups;

use Yii;
use common\models\media\Media;
use common\models\media\MediaCategories;
use common\models\teacher\Teacher;

/**
 * This is the model class for table "modules_groups".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $gallery_id
 * @property string $txt_1
 * @property string $txt_2
 * @property string $txt_3
 * @property string $txt_4
 * @property int $teacher
 * @property int $teacher_second
 */
class Groups extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'modules_groups';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['description', 'slug'], 'string'],
            [['gallery_id', 'gallery_public', 'teacher', 'teacher_second'], 'integer'],
            [['title', 'txt_1', 'txt_2', 'txt_3', 'txt_4'], 'string', 'max' => 255],
            [['image'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['image' => 'id']],
            [['teacher'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['teacher' => 'id']],
            [['teacher_second'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['teacher_second' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('groups', 'ID'),
            'title' => Yii::t('groups', 'Title'),
            'description' => Yii::t('groups', 'Description'),
            'gallery_id' => Yii::t('groups', 'Gallery ID'),
            'gallery_public' => Yii::t('groups', 'Gallery Public'),
            'txt_1' => Yii::t('groups', 'Txt 1'),
            'txt_2' => Yii::t('groups', 'Txt 2'),
            'txt_3' => Yii::t('groups', 'Txt 3'),
            'txt_4' => Yii::t('groups', 'Txt 4'),
            'teacher' => Yii::t('groups', 'Teacher'),
            'teacher_second' => Yii::t('groups', 'Teacher Second'),
            'slug' => Yii::t('groups', 'Slug'),
            'image' => Yii::t('groups', 'Thumbnail'),
        ];
    }

    public function getTeacher() {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher']);
    }

    public function getTeacherSecond() {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_second']);
    }

    public function getMedia() {
        return $this->hasOne(Media::className(), ['id' => 'image']);
    }
    public function hasMedia() {
        return !is_null($this->hasOne(Media::className(), ['id' => 'image']));
    }

    public function getGallery() {
        return $this->hasOne(MediaCategories::className(), ['id' => 'gallery_id']);
    }

    public function getGalleryPublic() {
        return $this->hasOne(MediaCategories::className(), ['id' => 'gallery_public']);
    }

    /**
     * @inheritdoc
     * @return GroupsQuery the active query used by this AR class.
     */
    public static function find() {
        return new GroupsQuery(get_called_class());
    }

}
