<?php

namespace common\models\groups;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\groups\Groups;

/**
 * GroupsSearch represents the model behind the search form of `common\models\groups\Groups`.
 */
class GroupsSearch extends Groups {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'gallery_id', 'gallery_public', 'teacher', 'teacher_second'], 'integer'],
            [['title', 'description', 'txt_1', 'txt_2', 'txt_3', 'txt_4', 'slug'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Groups::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gallery_id' => $this->gallery_id,
            'gallery_public' => $this->gallery_public,
            'teacher' => $this->teacher,
            'teacher_second' => $this->teacher_second,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'txt_1', $this->txt_1])
                ->andFilterWhere(['like', 'txt_2', $this->txt_2])
                ->andFilterWhere(['like', 'txt_3', $this->txt_3])
                ->andFilterWhere(['like', 'slug', $this->slug])
                ->andFilterWhere(['like', 'txt_4', $this->txt_4]);

        return $dataProvider;
    }

}
