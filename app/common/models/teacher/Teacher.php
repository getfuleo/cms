<?php

namespace common\models\teacher;

use Yii;
use common\models\media\Media;
use common\models\groups\Groups;

/**
 * This is the model class for table "modules_teacher".
 *
 * @property int $id
 * @property int $image
 * @property string $title
 * @property string $subtitle
 * @property string $description
 * @property int $group
 * @property int $active
 *
 * @property ModulesMediaCategories $image0
 */
class Teacher extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'modules_teacher';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['image', 'group', 'active'], 'integer'],
            [['description','slug'], 'string'],
            [['title', 'subtitle'], 'string', 'max' => 255],
            [['image'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['image' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('teacher', 'ID'),
            'image' => Yii::t('teacher', 'Image'),
            'title' => Yii::t('teacher', 'Title'),
            'subtitle' => Yii::t('teacher', 'Subtitle'),
            'description' => Yii::t('teacher', 'Description'),
            'group' => Yii::t('teacher', 'Group'),
            'actice' => Yii::t('teacher', 'Active'),
            'slug' => Yii::t('teacher', 'Slug'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedia() {
        return $this->hasOne(Media::className(), ['id' => 'image']);
    }
    public function hasMedia() {
        return !is_null($this->hasOne(Media::className(), ['id' => 'image']));
    }

    public function getImage() {
        return $this->hasOne(Media::className(), ['id' => 'image']);
    }

    public function getGroups() {
        return $this->hasOne(Groups::className(), ['id' => 'group']);
    }

    /**
     * @inheritdoc
     * @return TeacherQuery the active query used by this AR class.
     */
    public static function find() {
        return new TeacherQuery(get_called_class());
    }

}
