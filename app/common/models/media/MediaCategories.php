<?php

namespace common\models\media;

use Yii;

/**
 * This is the model class for table "modules_media_categories".
 *
 * @property int $id
 * @property string $title
 * @property int $parent_id
 *
 * @property ModulesMedia[] $modulesMedia
 */
class MediaCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modules_media_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['title', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('media', 'ID'),
            'title' => Yii::t('media', 'Title'),
            'parent_id' => Yii::t('media', 'Parent ID'),
            'slug' => Yii::t('media', 'Slug'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedia()
    {
        return $this->hasMany(Media::className(), ['categories_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return MediaCategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MediaCategoriesQuery(get_called_class());
    }
}
