<?php

namespace common\models\media;

/**
 * This is the ActiveQuery class for [[MediaCategories]].
 *
 * @see MediaCategories
 */
class MediaCategoriesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return MediaCategories[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MediaCategories|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
