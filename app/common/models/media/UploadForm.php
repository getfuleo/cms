<?php

namespace common\models\media;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\imagine\Image;
class UploadForm extends Model {

    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $categories_id;
    public $title;

    public function rules() {
        return [
                [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, jpeg, doc, docx, xls, xlsx, pdf, zip, rar', "maxFiles" => 10],
                [['categories_id', 'size'], 'integer'],
                [['title'], 'string']
        ];
    }

    public function upload() {

        $name = md5(time()) . '.' . $this->imageFile->extension;
        $save = $this->imageFile->saveAs(Yii::getAlias('@root/upload/') . $name);


        /*
         * thumbnail 16:9 ratio
         */
        Image::thumbnail('@root/upload/' . $name, 800, 450)->save(Yii::getAlias('@root/upload/thumb/large/' . $name), ['quality' => 80]);
        Image::thumbnail('@root/upload/' . $name, 640, 360)->save(Yii::getAlias('@root/upload/thumb/medium/' . $name), ['quality' => 80]);
        Image::thumbnail('@root/upload/' . $name, 300, 169)->save(Yii::getAlias('@root/upload/thumb/small/' . $name), ['quality' => 80]);
        //Image::crop('@root/upload/'.$file, 120, 120)->save(Yii::getAlias('@root/upload/thumb/'.$file), ['quality' => 80]);


        if ($save) {
            return $name;
        } else {
            false;
        }
    }

}
