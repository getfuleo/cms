<?php

namespace common\models\media;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "modules_media".
 *
 * @property int $id
 * @property int $categories_id
 * @property string $name
 * @property string $size
 * @property string $url
 * @property string $thumbnailUrl
 * @property string $type
 *
 * @property ModulesMediaCategories $categories
 */
class Media extends \yii\db\ActiveRecord {



    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'modules_media';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                
                [['categories_id','size'], 'integer'],
                [['name', 'url', 'thumbnailUrl', 'type','title','slug'], 'string', 'max' => 255],
                [['categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => MediaCategories::className(), 'targetAttribute' => ['categories_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('media', 'ID'),
            'categories_id' => Yii::t('media', 'Categories ID'),
            'name' => Yii::t('media', 'Name'),
            'size' => Yii::t('media', 'Size'),
            'url' => Yii::t('media', 'Url'),
            'thumbnailUrl' => Yii::t('media', 'Thumbnail Url'),
            'type' => Yii::t('media', 'Type'),
            'imageFile' => Yii::t('media', 'Image File'),
            'title' => Yii::t('media', 'Title'),
            'slug' => Yii::t('media', 'Slug'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories() {
        return $this->hasOne(MediaCategories::className(), ['id' => 'categories_id']);
    }



    /**
     * @inheritdoc
     * @return MediaQuery the active query used by this AR class.
     */
    public static function find() {
        return new MediaQuery(get_called_class());
    }

}
