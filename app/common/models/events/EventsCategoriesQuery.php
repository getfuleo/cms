<?php

namespace common\models\events;

/**
 * This is the ActiveQuery class for [[EventsCategories]].
 *
 * @see EventsCategories
 */
class EventsCategoriesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return EventsCategories[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EventsCategories|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
