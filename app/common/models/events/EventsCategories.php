<?php

namespace common\models\events;

use Yii;

/**
 * This is the model class for table "modules_events_categories".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $id_parent
 * @property string $description_short
 * @property string $date_create
 * @property string $date_update
 * @property integer $user_create
 * @property integer $user_update
 * @property integer $active
 *
 * @property ModulesEvents[] $modulesEvents
 */
class EventsCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modules_events_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'description_short', 'date_create', 'date_update', 'user_create', 'user_update', 'active'], 'required'],
            [['description', 'description_short'], 'string'],
            [['id_parent', 'user_create', 'user_update', 'active'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('events', 'ID'),
            'title' => Yii::t('events', 'Title'),
            'description' => Yii::t('events', 'Description'),
            'id_parent' => Yii::t('events', 'Id Parent'),
            'description_short' => Yii::t('events', 'Description Short'),
            'date_create' => Yii::t('events', 'Date Create'),
            'date_update' => Yii::t('events', 'Date Update'),
            'user_create' => Yii::t('events', 'User Create'),
            'user_update' => Yii::t('events', 'User Update'),
            'active' => Yii::t('events', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModulesEvents()
    {
        return $this->hasMany(ModulesEvents::className(), ['categories_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return EventsCategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EventsCategoriesQuery(get_called_class());
    }
}
