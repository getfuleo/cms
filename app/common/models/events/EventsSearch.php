<?php

namespace common\models\events;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\events\Events;

/**
 * EventsSearch represents the model behind the search form about `common\models\events\Events`.
 */
class EventsSearch extends Events
{
    public $month;
    public $year;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_create', 'user_update', 'active', 'categories_id', 'slug','month','year'], 'integer'],
            [['title', 'description', 'description_short', 'date_create', 'date_update', 'date_at', 'date_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Events::find();

        // add conditions that should always apply here
         if (isset($params['EventsSearch'])) {

            $month = intval($params['EventsSearch']['month']);
            $year = intval($params['EventsSearch']['year']);
     
            if ($month) {
                $query->andWhere(['=', 'MONTH(date_at)', $month]);
            }
            
            if ($year) {
                $query->andWhere(['=', 'YEAR(date_at)', $year]);
            }
            
            
         }
         
//         var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
//exit();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
            'user_create' => $this->user_create,
            'user_update' => $this->user_update,
            'active' => $this->active,
            'categories_id' => $this->categories_id,
            'date_at' => $this->date_at,
            'date_to' => $this->date_to,
            'slug' => $this->slug,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'description_short', $this->description_short]);

        return $dataProvider;
    }
}
