<?php

namespace common\models\events;

use Yii;

/**
 * This is the model class for table "modules_events".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $description_short
 * @property string $date_create
 * @property string $date_update
 * @property integer $user_create
 * @property integer $user_update
 * @property integer $active
 * @property integer $categories_id
 * @property string $date
 * @property integer $slug
 * @property integer $date_show
 *
 * @property ModulesEventsCategories $categories
 */
class Events extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'modules_events';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['description', 'description_short'], 'string'],
                [['date_create', 'date_update', 'date_at', 'date_to'], 'safe'],
                [['user_create', 'user_update', 'active', 'categories_id', 'slug','date_show'], 'integer'],
                [['title'], 'string', 'max' => 255],
                [['categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventsCategories::className(), 'targetAttribute' => ['categories_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('events', 'ID'),
            'title' => Yii::t('events', 'Title'),
            'description' => Yii::t('events', 'Description'),
            'description_short' => Yii::t('events', 'Description Short'),
            'date_create' => Yii::t('events', 'Date Create'),
            'date_update' => Yii::t('events', 'Date Update'),
            'user_create' => Yii::t('events', 'User Create'),
            'user_update' => Yii::t('events', 'User Update'),
            'active' => Yii::t('events', 'Active'),
            'categories_id' => Yii::t('events', 'Categories ID'),
            'date_at' => Yii::t('events', 'Date At'),
            'date_to' => Yii::t('events', 'Date To'),
            'slug' => Yii::t('events', 'Slug'),
            'date_show' => Yii::t('events', 'Date Show'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories() {
        return $this->hasOne(Categories::className(), ['id' => 'categories_id']);
    }

    public function findMonth($month) {
        foreach (Events::months() as $key => $val) {
            if ($val['slug'] === $month || $month == $val['id']) {
                return $val;
            }
        }
        return null;
    }

    public function months() {
        $months = [
                [
                'id' => 1,
                'slug' => Yii::t('month', 'january-slug'),
                'name' => Yii::t('month', 'january')
            ],
                [
                'id' => 2,
                'slug' => Yii::t('month', 'february-slug'),
                'name' => Yii::t('month', 'february')
            ],
                [
                'id' => 3,
                'slug' => Yii::t('month', 'march-slug'),
                'name' => Yii::t('month', 'march')
            ],
                [
                'id' => 4,
                'slug' => Yii::t('month', 'april-slug'),
                'name' => Yii::t('month', 'april')
            ],
                [
                'id' => 5,
                'slug' => Yii::t('month', 'may-slug'),
                'name' => Yii::t('month', 'may')
            ],
                [
                'id' => 6,
                'slug' => Yii::t('month', 'june-slug'),
                'name' => Yii::t('month', 'june')
            ],
                [
                'id' => 7,
                'slug' => Yii::t('month', 'july-slug'),
                'name' => Yii::t('month', 'july')
            ],
                [
                'id' => 8,
                'slug' => Yii::t('month', 'august-slug'),
                'name' => Yii::t('month', 'august')
            ],
                [
                'id' => 9,
                'slug' => Yii::t('month', 'september-slug'),
                'name' => Yii::t('month', 'september')
            ],
             [
                'id' => 10,
                'slug' => Yii::t('month', 'october-slug'),
                'name' => Yii::t('month', 'october')
            ],
                [
                'id' => 11,
                'slug' => Yii::t('month', 'november-slug'),
                'name' => Yii::t('month', 'november')
            ],
               
                [
                'id' => 12,
                'slug' => Yii::t('month', 'december-slug'),
                'name' => Yii::t('month', 'december')
            ]
        ];
        return $months;
    }

    /**
     * @inheritdoc
     * @return EventsQuery the active query used by this AR class.
     */
    public static function find() {
        return new EventsQuery(get_called_class());
    }

}
