<?php

namespace common\models\articles;

use Yii;

/**
 * This is the model class for table "modules_articles_categories".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $id_parent
 * @property string $description_short
 * @property string $date_create
 * @property string $date_update
 * @property integer $user_create
 * @property integer $user_update
 * @property integer $active
 * @property integer $slug
 *
 * @property ModulesArticles[] $modulesArticles
 */
class ArticlesCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modules_articles_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description', 'description_short', 'slug'], 'string'],
            [['id_parent', 'user_create', 'user_update', 'active'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('articles', 'ID'),
            'title' => Yii::t('articles', 'Title'),
            'description' => Yii::t('articles', 'Description'),
            'id_parent' => Yii::t('articles', 'Id Parent'),
            'description_short' => Yii::t('articles', 'Description Short'),
            'date_create' => Yii::t('articles', 'Date Create'),
            'date_update' => Yii::t('articles', 'Date Update'),
            'user_create' => Yii::t('articles', 'User Create'),
            'user_update' => Yii::t('articles', 'User Update'),
            'active' => Yii::t('articles', 'Active'),
            'slug' => Yii::t('articles', 'Slug'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Articles::className(), ['categories_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ArticlesCategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticlesCategoriesQuery(get_called_class());
    }
}
