<?php

namespace common\models\articles;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\articles\Articles;

/**
 * ArticlesSearch represents the model behind the search form about `common\models\articles\Articles`.
 */
class ArticlesSearch extends Articles {

   // public $categories;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['id', 'categories_id', 'active', 'user_create', 'user_update'], 'integer'],
                [['title', 'description', 'description_short', 'date_create', 'date_update', 'url','slug'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Articles::find();

        // add conditions that should always apply here
        //$query->joinWith(['categories']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
//        $dataProvider->sort->attributes['categories'] = [
//            'asc' => ['modules_articles_categories.title' => SORT_ASC],
//            'desc' => ['modules_articles_categories.title' => SORT_DESC],
//        ];


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'categories_id' => $this->categories_id,
            'active' => $this->active,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
            'user_create' => $this->user_create,
            'user_update' => $this->user_update,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'description_short', $this->description_short])
                ->andFilterWhere(['like', 'url', $this->description_short])
                 ->andFilterWhere(['like', 'slug', $this->slug]);
               // ->andFilterWhere(['like', 'modules_articles_categories.title', $this->categories]);

        return $dataProvider;
    }

}
