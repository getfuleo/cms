<?php

namespace common\models\articles;

use Yii;
use yii\behaviors\SluggableBehavior;
use common\models\media\Media;

/**
 * This is the model class for table "modules_articles".
 *
 * @property integer $id
 * @property integer $categories_id
 * @property string $title
 * @property string $description
 * @property string $description_short
 * @property integer $active
 * @property string $date_create
 * @property string $date_update
 * @property integer $user_create
 * @property integer $user_update
 * @property integer $attachment_1
 * @property integer $url
 * @property integer $data
 *
 * @property ModulesArticlesCategories $categories
 */
class Articles extends \yii\db\ActiveRecord {

   // public $data = [];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'modules_articles';
    }

    public function behaviors() {
        return [
                [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['title'], 'required'],
                [['categories_id', 'active', 'user_create', 'user_update'], 'integer'],
                [['description', 'description_short', 'url'], 'string'],
                [['date_create', 'date_update','image','data'], 'safe'],
                [['title'], 'string', 'max' => 255],
                [['categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticlesCategories::className(), 'targetAttribute' => ['categories_id' => 'id']],
                [['image'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['image' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('articles', 'ID'),
            'categories_id' => Yii::t('articles', 'Categories ID'),
            'title' => Yii::t('articles', 'Title'),
            'description' => Yii::t('articles', 'Description'),
            'description_short' => Yii::t('articles', 'Description Short'),
            'active' => Yii::t('articles', 'Active'),
            'date_create' => Yii::t('articles', 'Date Create'),
            'date_update' => Yii::t('articles', 'Date Update'),
            'user_create' => Yii::t('articles', 'User Create'),
            'user_update' => Yii::t('articles', 'User Update'),
            'slug' => Yii::t('articles', 'Slug'),
            'categories' => Yii::t('articles', 'Categories'),
            'url' => Yii::t('articles', 'Url'),
            'image' => Yii::t('articles', 'Image'),
            'data' => Yii::t('articles', 'Data'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories() {
        return $this->hasOne(ArticlesCategories::className(), ['id' => 'categories_id']);
    }
    public function getMedia() {
        return $this->hasOne(Media::className(), ['id' => 'image'])->one()['name'];
    }
 
   
    /**
     * @inheritdoc
     * @return ArticlesQuery the active query used by this AR class.
     */
    public static function find() {
        return new ArticlesQuery(get_called_class());
    }

}
