<?php

namespace common\models\modules;

use Yii;

/**
 * This is the model class for table "modules".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $active
 * @property string $class
 * @property string $namespace
 * @property string $namespace_child
 *
 * @property Block[] $blocks
 */
class Modules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['active'], 'integer'],
            [['title', 'class', 'namespace',  'namespace_child'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('modules', 'ID'),
            'title' => Yii::t('modules', 'Title'),
            'description' => Yii::t('modules', 'Description'),
            'active' => Yii::t('modules', 'Active'),
            'class' => Yii::t('modules', 'Class'),
            'namespace' => Yii::t('modules', 'Namespace'),
            'namespace_child' => Yii::t('modules', 'Namespace Child'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlocks()
    {
        return $this->hasMany(Block::className(), ['module_id' => 'id']);
    }
    
     public function getNamespace()
    {
        return $this->namespace;
    }
     public function getNamespaceChild()
    {
        return $this->namespace_child;
    }

    /**
     * @inheritdoc
     * @return ModulesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ModulesQuery(get_called_class());
    }
}
