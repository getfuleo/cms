<?php

namespace common\models\block;

use Yii;
use common\models\modules\Modules;

/**
 * This is the model class for table "block".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $data
 * @property int $sort
 * @property int $site_id
 * @property int $module_id
 * @property int $data_id
 * @property string $date_created
 * @property string $date_updated
 * @property int $active
 * @property int $row
 * @property int $col
 * @property string $layout
 * @property int $limit
 * @property int $slug
 *
 * @property Modules $module
 */
class Block extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'block';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['site_id', 'module_id', 'active','row','col'], 'required'],
            
            
            [['description', 'data', 'slug', 'order'], 'string'],
            [['sort', 'site_id', 'module_id', 'data_id', 'active', 'row', 'col','limit_data'], 'integer'],
            
            [['date_created', 'date_updated'], 'safe'],
            [['title', 'col', 'layout'], 'string', 'max' => 255],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modules::className(), 'targetAttribute' => ['module_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('block', 'ID'),
            'title' => Yii::t('block', 'Title'),
            'description' => Yii::t('block', 'Description'),
            'data' => Yii::t('block', 'Data'),
            'sort' => Yii::t('block', 'Sort'),
            'site_id' => Yii::t('block', 'Site ID'),
            'module_id' => Yii::t('block', 'Module ID'),
            'data_id' => Yii::t('block', 'Data ID'),
            'date_created' => Yii::t('block', 'Date Created'),
            'date_updated' => Yii::t('block', 'Date Updated'),
            'active' => Yii::t('block', 'Active'),
            'row' => Yii::t('block', 'Row'),
            'col' => Yii::t('block', 'Col'),
            'layout' => Yii::t('block', 'Layout'),
            'limit_data' => Yii::t('block', 'Limit Data'),
            'slug' => Yii::t('block', 'Slug'),
            'order' => Yii::t('block', 'Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Modules::className(), ['id' => 'module_id']);
    }
    
    
        

    /**
     * @inheritdoc
     * @return BlockQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BlockQuery(get_called_class());
    }
}
