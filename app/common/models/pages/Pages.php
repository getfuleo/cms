<?php

namespace common\models\pages;

use Yii;
use common\models\block\Block;
use common\models\modules\Modules;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $slug
 * @property string $title
 * @property string $description
 * @property integer $active
 * @property string $data
 * @property integer $access
 */
class Pages extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['slug', 'title'], 'required'],
                [['id', 'active', 'access'], 'integer'],
                [['description', 'data', 'type'], 'string'],
                [['slug', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('pages', 'ID'),
            'slug' => Yii::t('pages', 'Slug'),
            'title' => Yii::t('pages', 'Title'),
            'description' => Yii::t('pages', 'Description'),
            'active' => Yii::t('pages', 'Active'),
            'data' => Yii::t('pages', 'Data'),
            'access' => Yii::t('pages', 'Access'),
            'type' => Yii::t('pages', 'type'),
        ];
    }

    public function getBlock($slug = false) {

        if ($slug) {
            $result = Block::findOne(['slug' => $slug]);
        } else {
            $result = Block::find()->where(['site_id' => $this->id])->all();
        }
        return $result;
    }

    /**
     * @inheritdoc
     * @return PagesQuery the active query used by this AR class.
     */
    public static function find() {
        return new PagesQuery(get_called_class());
    }

}
