/*
 *
 * Management Gulp file
 *
 * @author: Tomasz Załucki <tomek@fuleo.pl>
 *
 */
var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var promise = require('es6-promise').polyfill();
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var gulpMerge = require('gulp-merge');
var minify = require('gulp-minify');


var csso = require('gulp-csso');
var buffer = require('vinyl-buffer');
var imagemin = require('gulp-imagemin');
var merge = require('merge-stream');
var spritesmith = require('gulp.spritesmith');

var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

const assetsDir = "assets/";
const assetsDist = '../web/static/';




gulp.task('sprite', function () {
    // Generate our spritesheet 
    var spriteData = gulp.src(assetsDir + 'img/icons/*.png').pipe(spritesmith({
        imgName: '../img/icons.png',
        cssName: 'icons.css'


    }));

    // Pipe image stream through image optimizer and onto disk 
    var imgStream = spriteData.img
            // DEV: We must buffer our stream into a Buffer for `imagemin` 
            .pipe(buffer())
            .pipe(imagemin())
            .pipe(gulp.dest(assetsDist + 'img'));

    // Pipe CSS stream through CSS optimizer and onto disk 
    var cssStream = spriteData.css
            .pipe(csso())
            .pipe(gulp.dest(assetsDir + 'css'));

    // Return a merged stream to handle both `end` events 
    return merge(imgStream, cssStream);
});

gulp.task('sprite-logo', function () {
    // Generate our spritesheet 
    var spriteData = gulp.src(assetsDir + 'img/logo/*.png').pipe(spritesmith({
        imgName: 'logo.png',
        cssName: 'logo.css'
    }));

    // Pipe image stream through image optimizer and onto disk 
    var imgStream = spriteData.img
            // DEV: We must buffer our stream into a Buffer for `imagemin` 
            .pipe(buffer())
            .pipe(imagemin())
            .pipe(gulp.dest(assetsDist + 'img'));

    // Pipe CSS stream through CSS optimizer and onto disk 
    var cssStream = spriteData.css
            .pipe(csso())
            .pipe(gulp.dest(assetsDir + 'css'));

    // Return a merged stream to handle both `end` events 
    return merge(imgStream, cssStream);
});




gulp.task('sass', function () {
    return gulp.src([assetsDir + 'scss/*.scss', assetsDir + 'css/*.css'])
            .pipe(sourcemaps.init())
            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
            //.pipe(sourcemaps.write(assetsDist+'css/maps'))
            .pipe(postcss([autoprefixer()]))
            .pipe(concat('style.css'))
            .pipe(gulp.dest(assetsDist + 'css'));
});


gulp.task('js', function () {
    return gulpMerge(
            gulp.src(assetsDir + 'js/*.js')
            .pipe(uglify())
            .pipe(concat('script.js'))
            .pipe(gulp.dest(assetsDist + 'js'))
            )
});

// node_modules JS
gulp.task('js-library', function () {

    return gulpMerge(
            gulp.src([
               // 'node_modules/jquery/dist/jquery.min.js',

                'node_modules/popper.js/dist/umd/popper.min.js',
                'node_modules/bootstrap/dist/js/bootstrap.min.js',
                'node_modules/summernote/dist/summernote-bs4.min.js',
                'node_modules/select2/dist/js/select2.min.js',
                assetsDir +'js/modules/jquery-ui-1.12.1/jquery-ui.min.js',
                assetsDir +'js/modules/jquery-ui-1.12.1/jquery-ui-timepicker-addon.js'





            ])

            .pipe(concat('library.js'))
            .pipe(gulp.dest(assetsDist + 'js'))
            )
});

// node_modules CSS
gulp.task('css-library', function () {

    return gulpMerge(
            gulp.src([
                'node_modules/bootstrap/dist/css/bootstrap.min.css',
                'node_modules/summernote/dist/summernote-bs4.css',
                 'node_modules/select2/dist/css/select2.min.css',
                 assetsDir +'js/modules/jquery-ui-1.12.1/jquery-ui.min.css',
                 assetsDir +'js/modules/jquery-ui-1.12.1/jquery-ui-timepicker-addon.css',


            ])
            .pipe(concat('library.css'))
            .pipe(gulp.dest(assetsDist + 'css'))
            )
});
gulp.task('hack', function () {

    return gulpMerge(
            gulp.src(assetsDir + 'js/hack/*.js')
            .pipe(uglify())
            .pipe(concat('hack.js'))
            .pipe(gulp.dest(assetsDist + 'js'))
            )

});

// one task
gulp.task('sass:watch', function () {
    gulp.watch(assetsDir + 'scss/**/*.scss', ['sass']);
});

gulp.task('js:watch', function () {
    gulp.watch(assetsDir + 'js/**/*.js', ['js']);
});


//first developer task
gulp.task('first', ['sprite', 'sprite-logo', 'sass', 'js', 'hack', 'js-library', 'css-library']);

// multi task
gulp.task('watch', function () {
    gulp.watch(assetsDir + 'scss/**/*.scss', ['sass']);
    gulp.watch(assetsDir + 'js/**/*.js', ['js']);
});