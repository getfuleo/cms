$(document).ready(function () {
//    var keys = $('#grid').yiiGridView('getSelectedRows');
    $.datepicker.regional['pl'] = {
        closeText: "Zamknij",
        prevText: "&#x3C;Poprzedni",
        nextText: "Następny&#x3E;",
        currentText: "Dziś",
        monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec",
            "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
        monthNamesShort: ["Sty", "Lu", "Mar", "Kw", "Maj", "Cze",
            "Lip", "Sie", "Wrz", "Pa", "Lis", "Gru"],
        dayNames: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
        dayNamesShort: ["Nie", "Pn", "Wt", "Śr", "Czw", "Pt", "So"],
        dayNamesMin: ["N", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
        weekHeader: "Tydz",
        dateFormat: "yy-mm-dd",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ""};
    $.datepicker.setDefaults($.datepicker.regional['pl']);

    $.timepicker.regional['pl'] = {
        timeOnlyTitle: 'Выберите время',
        timeText: 'Czas',
        hourText: 'Godzina',
        minuteText: 'Minuty',
        secondText: 'Sekundy',
        millisecText: 'Milisekundy',
        timezoneText: 'Strefa czasowa',
        currentText: 'Obecny',
        closeText: 'Zamknij',
        timeFormat: 'HH:mm:ss',
        amNames: ['AM', 'A'],
        pmNames: ['PM', 'P'],
        isRTL: false
    };
    $.timepicker.setDefaults($.timepicker.regional['pl']);

    $('#event-datepicker-at').datetimepicker();
    $('#event-datepicker-to').datetimepicker();


    $('textarea').summernote({
        //placeholder: 'Hello bootstrap 4', 

        tabsize: 2,
        height: 200
    });


    $('select').select2();

    $('select').change(function () {
        $('#block-slug').val($(this).find(":selected").parent()[0].label);
    });

    $('[data-action="image"]').change(function () {
        var id = $(this).find(":selected").val();
        $.get("/admin/media/model?id=" + id, function (data) {
            data = JSON.parse(data);
            $('[ data-item="image-ajax"]').after().html('<img src="/upload/thumb/large/' + data.name + '" class="img-thumbnail"/>');
        });
    });

}); 