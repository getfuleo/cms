<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\groups\Groups;
use common\models\media\Media;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\teacher\Teacher */
/* @var $form yii\widgets\ActiveForm */
?>




<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-8">
        <div class="card">
            <h4 class="card-header"><?= Yii::t('general', 'Panel Content') ?></h4>
            <div class="card-body">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'subtitle')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('articles', 'Create') : Yii::t('articles', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
      
    </div>
    <div class="col-4">
        <div class="card">
            <h4 class="card-header"><?= Yii::t('general', 'Panel Settings') ?></h4>
            <div class="card-body">
                <?= $form->field($model, 'group')->dropDownList(ArrayHelper::map(Groups::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'select group')]); ?>
                <?= $form->field($model, 'active')->dropDownList([1 => Yii::t('general', 'active'), 0 => Yii::t('general', 'inactive')]) ?>
      
            </div>
        </div>
        
        
           <div class="card mt-3">
            <h4 class="card-header"><?= Yii::t('general', 'Thumbnail') ?></h4>
            <div class="card-body">
                <?= $form->field($model, 'image')->dropDownList(ArrayHelper::map(Media::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'select image'), 'data-action' => 'image']); ?>
                <div data-item="image-ajax">
                    <?php if ($model->image) { ?>
                        <img src="/upload/<?= $model->getMedia()->one()->name ?>" class="img-thumbnail"/>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>



<?php ActiveForm::end(); ?>
