<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\teacher\Teacher */

$this->title = Yii::t('teacher', 'Create Teacher');
$this->params['breadcrumbs'][] = ['label' => Yii::t('teacher', 'Teachers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teacher-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
