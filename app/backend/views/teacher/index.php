<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\groups\Groups;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TeacherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('teacher', 'Teachers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teacher-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
<?= Html::a(Yii::t('teacher', 'Create Teacher'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'subtitle',
            [
                'attribute' => 'group',
                'value' => 'groups.title',
                'filter' => ArrayHelper::map(Groups::find()->asArray()->all(), 'id', 'title'),
            ],
//             [
//                'attribute' => 'url',
//                  'contentOptions' => ['style' => 'width: 145px;'],
//                'value' => function($model){
//                return Html::img('/upload/thumb/small/'.$model->name,['class'=>'img-thumbnail']);
//                },
//                 'format' => 'raw'
//                //'filter' => ArrayHelper::map(MediaCategories::find()->asArray()->all(), 'id', 'title'),
//            ],
            //'thumbnailUrl',
            //'type',
            ['class' => 'common\hooks\yii2\grid\ActionColumn'],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
