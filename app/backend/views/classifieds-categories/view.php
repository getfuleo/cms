<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\classifieds\ClassifiedsCategories */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('classifieds', 'Classifieds Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classifieds-categories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::a(Yii::t('classifieds', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('classifieds', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('classifieds', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'descripiton:ntext',
            'id_parent',
            'description_short:ntext',
            'date_create',
            'date_update',
            'user_create',
            'user_update',
            'active',
        ],
    ]) ?>

</div>
