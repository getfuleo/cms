<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\classifieds\ClassifiedsCategories */

$this->title = Yii::t('classifieds', 'Create Classifieds Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('classifieds', 'Classifieds Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classifieds-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?></p>
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
