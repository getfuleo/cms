<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\classifieds\ClassifiedsCategories */

$this->title = Yii::t('classifieds', 'Update {modelClass}: ', [
    'modelClass' => 'Classifieds Categories',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('classifieds', 'Classifieds Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('classifieds', 'Update');
?>
<div class="classifieds-categories-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?></p>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
