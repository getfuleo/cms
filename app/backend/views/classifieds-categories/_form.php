<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\classifieds\ClassifiedsCategories;

/* @var $this yii\web\View */
/* @var $model common\models\classifieds\ClassifiedsCategories */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-8">
        <div class="card">
            <h4 class="card-header"><?= Yii::t('general', 'Panel Content') ?></h4>
            <div class="card-body">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'description_short')->textarea(['rows' => 6]) ?>
                <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('articles', 'Create') : Yii::t('articles', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="card">
            <h4 class="card-header"><?= Yii::t('general', 'Panel Settings') ?></h4>
            <div class="card-body">

                <?= $form->field($model, 'id_parent')->dropDownList(ArrayHelper::map(ClassifiedsCategories::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'Without Parent')]); ?>
                <?= $form->field($model, 'active')->dropDownList([1 => Yii::t('general', 'active'), 0 => Yii::t('general', 'inactive')]) ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>