<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\classifieds\ClassifiedsCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('classifieds', 'Classifieds Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classifieds-categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('classifieds', 'Create Classifieds Categories'), ['create'], ['class' => 'btn btn-success']) ?>
         <?= Html::a(Yii::t('classifieds', 'Classifieds'), ['classifieds/index'], ['class' => 'btn btn-dark']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description:ntext',
            'id_parent',
            'description_short:ntext',
            // 'date_create',
            // 'date_update',
            // 'user_create',
            // 'user_update',
            // 'active',

             ['class' => 'common\hooks\yii2\grid\ActionColumn']
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
