<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
<?php $this->head() ?>
    </head>
    <body>

<?php $this->beginBody() ?>
        <div class="d-flex flex-row main">
            <nav id="nav">
                <h1>
                    CMS.inn 
                    <a href="/admin/site/logout" title="Wyloguj się"><span class="float-right badge badge-dark"><i class="fa fa-power-off"></i></span></a>
                    <a href="/" title="Przedź do widoku strony"><span class="float-right badge badge-primary"><i class="fa fa-home"></i></span></a>
                </h1>
                <ul>
                    <li><a href="/admin/pages" title="">Podstrony</a> </li>
                    <li><a href="/admin/articles" title="">Treści</a></li>
                    <li><a href="/admin/events" title="">Wydarzenia</a></li>
                    <li><a href="/admin/classifieds" title="">Ogłoszenia</a></li>
                    <li><a href="/admin/groups" title="">Grupy</a></li>
                    <li><a href="/admin/media" title="">Media</a></li>
                    <li><a href="/admin/user" title="">Użytkownicy</a></li>
                    <li><a href="/admin/teacher" title="">Nauczyciele</a></li>
                </ul>
            </nav>
            <main>
                <div class="wrap d-flex align-items-top">
                    <div class="container-fluid">
                        <div class="row">
                            <?=
                            common\hooks\yii2\widgets\Breadcrumbs::widget([
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            ])
                            ?> 
                        </div>
                        <?= Alert::widget() ?>
                        <?= $content ?>
                    </div>
                </div>
            </main>
        </div>
<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>