<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\media\MediaCategories */

$this->title = Yii::t('events', 'Create Media Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('events', 'Media Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>
<p><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?></p>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
