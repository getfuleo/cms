<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\articles\ArticlesCategories;

/* @var $this yii\web\View */
/* @var $model common\models\pages\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
      
        <div class="col-12">
            <div class="card">
                <h4 class="card-header"><?= Html::encode($this->title) ?></h4>
                <div class="card-body">


                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>   

                    <?= $form->field($model, 'active')->dropDownList([1 => Yii::t('general', 'active'),0 => Yii::t('general', 'inactive')]) ?>

                    <?= $form->field($model, 'access')->dropDownList([0 => Yii::t('general', 'public'), 1 => Yii::t('general', 'login')]) ?>

<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>



                        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                    <div class="form-group">
<?= Html::submitButton($model->isNewRecord ? Yii::t('pages', 'Create') : Yii::t('pages', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>







                </div>
            </div>
        </div>



 </div>
<?php ActiveForm::end(); ?>

    </div>
