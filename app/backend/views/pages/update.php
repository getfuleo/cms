<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\pages\Pages */

$this->title = Yii::t('pages', 'Update {modelClass}: ', [
            'modelClass' => 'Pages',
        ]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('pages', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('pages', 'Update');
?>
<div class="pages-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('pages', 'Return'), ['index'], ['class' => 'btn btn-dark']) ?>

    </p>

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-8">
            <div class="card">
                <h4 class="card-header">
                    Bloki podstrony


                    <?= Html::a(Yii::t('pages', 'Add Block'), ['create-block', 'id' => $model->id], ['class' => 'btn btn-success pull-right']) ?>
                </h4>

                <div class="card-body">





                    <?php foreach ($blocks as $row) { ?>

                        <div class="block">
                            <div class="row">

                                <?php foreach ($row as $col) { ?>




                                    <div class="col-12 col-<?= $col['col']; ?>">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title"><?= $col['title']; ?></h4>
                                                <h6 class="card-subtitle mb-2 text-muted"><?= $col['slug']; ?></h6>
                                                <p class="card-text"><?= $col['description']; ?></p>
                                                <a href="update-block?id=<?= $col['id']; ?>" class="card-link btn btn-primary">Edytuj</a>

                                                <?php
                                                echo Html::a(Yii::t('backend', 'Delete'), ['delete-block', 'id' => $col['id']], [
                                                    'class' => 'card-link btn btn-danger',
                                                    'data' => [
                                                        'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                        'method' => 'post',
                                                    ],
                                                ])
                                                ?>
                                            </div>
                                        </div>


                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>







                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <h4 class="card-header"><?= Yii::t('general', 'Panel Settings') ?></h4>
                <div class="card-body">


                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>   

                    <?= $form->field($model, 'active')->dropDownList([0 => Yii::t('general', 'inactive'), 1 => Yii::t('general', 'active')]) ?>

                    <?= $form->field($model, 'access')->dropDownList([0 => Yii::t('general', 'public'), 1 => Yii::t('general', 'login')]) ?>
                    <?= $form->field($model, 'type')->dropDownList(['one' => Yii::t('general', 'Type One'), 'multipage' => Yii::t('general', 'Type Multipage')]) ?>

                    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>



                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('pages', 'Create') : Yii::t('pages', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>







                </div>
            </div>
        </div>



    </div>
    <?php ActiveForm::end(); ?>

</div>
