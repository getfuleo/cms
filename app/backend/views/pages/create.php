<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\pages\Pages */

$this->title = Yii::t('pages', 'Create Pages');
$this->params['breadcrumbs'][] = ['label' => Yii::t('pages', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?></p>
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
