<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\pages\Pages;
use common\models\modules\Modules;

/* @var $this yii\web\View */
/* @var $model common\models\pages\Pages */

$this->title = Yii::t('pages', 'Update {modelClass}: ', [
            'modelClass' => 'Block',
        ]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('pages', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('pages', 'Return'), ['update', 'id' => $model->site_id], ['class' => 'btn btn-dark']) ?>

    </p>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-8">
            <div class="card">
                <h4 class="card-header"><?= Html::encode($this->title) ?></h4>
                <div class="card-body">
                    <?= $form->field($model, 'title') ?>   
                    <?= $form->field($model, 'description') ?> 





                    <?php
                    $namespace = Modules::findOne($model->module_id)->namespace;
                    ?>


                    <div class="row">
                        <div class="col-4">
                            <?=
                            $form->field($model, 'data_id')->dropDownList(ArrayHelper::map($namespace::find()->all(), 'id', 'title', 'slug'), ["prompt" => Yii::t('pages', 'select pages')]);
                            ?>
                        </div>
                        <div class="col-4">
                            <?= $form->field($model, 'slug') ?> 
                        </div>
                        <div class="col-4">
                            <?= $form->field($model, 'order')->dropDownList([SORT_DESC => Yii::t('general', 'SORT DESC'), SORT_ASC => Yii::t('general', 'SORT ASC')]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4"><?= $form->field($model, 'limit_data') ?> </div>
                        <div class="col-4"><?= $form->field($model, 'active')->dropDownList([1 => Yii::t('general', 'active'), 0 => Yii::t('general', 'inactive')]) ?></div>
                        <div class="col-4"> <?=
                            $form->field($model, 'layout')->dropDownList(ArrayHelper::map($directories, 'basename', 'basename'), ["prompt" => Yii::t('pages', 'select pages')]);
                            ?></div>




                    </div>









                </div>
                <div class="card-footer">
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('pages', 'Create') : Yii::t('pages', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <h4 class="card-header"><?= Html::encode($this->title) ?></h4>
                <div class="card-body">

                    <?= $form->field($model, 'row') ?> 
                    <?=
                    $form->field($model, 'col')->dropDownList([
                        1 => '1',
                        2 => '2',
                        3 => '3',
                        4 => '4',
                        5 => '5',
                        6 => '6',
                        71 => '7',
                        81 => '8',
                        91 => '9',
                        10 => '10',
                        11 => '11',
                        12 => '12'
                    ])
                    ?>

                </div>
            </div>
        </div>




    </div>
    <?php ActiveForm::end(); ?>

</div>
