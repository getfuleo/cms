<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\pages\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('pages', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <h1><?= Html::encode($this->title) ?></h1>
<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
    <?= Html::a(Yii::t('pages', 'Create Pages'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model) {
            if (!$model->active) {
                return ['class' => 'table-danger'];
            }
        },
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
            'id',
            'slug',
            'title',
            [
                'attribute' => 'description',
                'format'    =>'raw'
            ],
            'active',
            // 'data:ntext',
            // 'access',
            ['class' => 'common\hooks\yii2\grid\ActionColumn']
        ],
    ]);
    ?>
<?php Pjax::end(); ?></div>
