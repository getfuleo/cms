<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\events\EventsCategories */

$this->title = Yii::t('events', 'Create Events Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('events', 'Events Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?></p>
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
