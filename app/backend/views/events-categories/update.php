<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\events\EventsCategories */

$this->title = Yii::t('events', 'Update Events Categories: {nameAttribute}', [
            'nameAttribute' => $model->title,
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('events', 'Events Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('events', 'Update');
?>
<div class="events-categories-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?></p>
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
