<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\events\EventsCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('events', 'Events Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('events', 'Create Events Categories'), ['create'], ['class' => 'btn btn-success']) ?>
         <?= Html::a(Yii::t('events', 'Events'), ['events/index'], ['class' => 'btn btn-dark']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description:ntext',
            'id_parent',
            'description_short:ntext',
            // 'date_create',
            // 'date_update',
            // 'user_create',
            // 'user_update',
            // 'active',

             ['class' => 'common\hooks\yii2\grid\ActionColumn']
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
