<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\articles\ArticlesCategories */

$this->title = Yii::t('articles', 'Update Articles Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('articles', 'Articles Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="articles-categories-update">

    <h1><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?> <?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
