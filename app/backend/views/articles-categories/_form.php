<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\articles\ArticlesCategories;

/* @var $this yii\web\View */
/* @var $model common\models\articles\ArticlesCategories */
/* @var $form yii\widgets\ActiveForm */
?>



<?php $form = ActiveForm::begin(); ?>


<div class="row">
    <div class="col-12 col-lg-8 mb-5">
        <div class="card">
            <h4 class="card-header"><?= Yii::t('general', 'Panel Content') ?></h4>
            <div class="card-body">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>


                    <?= $form->field($model, 'description_short')->textarea(['rows' => 6]) ?>
                <div class="form-group">
<?= Html::submitButton($model->isNewRecord ? Yii::t('articles', 'Create') : Yii::t('articles', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>


    <div class="col-12  col-lg-4">
        <div class="card">
            <h4 class="card-header"><?= Yii::t('general', 'Panel Settings') ?></h4>
            <div class="card-body">

<?= $form->field($model, 'id_parent')->dropDownList(ArrayHelper::map(ArticlesCategories::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'select category')]); ?>


<?= $form->field($model, 'active')->dropDownList([0 => Yii::t('general', 'inactive'), 1 => Yii::t('general', 'active')]) ?>




            </div>
        </div>
    </div>

</div>
<?php ActiveForm::end(); ?>


