<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\articles\ArticlesCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('articles', 'Articles Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('articles', 'Create Articles Categories'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('articles', 'Articles'), ['articles/index'], ['class' => 'btn btn-dark']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description:ntext',
            'id_parent',
            'description_short:ntext',
            // 'date_create',
            // 'date_update',
            // 'user_create',
            // 'user_update',
            // 'active',

             ['class' => 'common\hooks\yii2\grid\ActionColumn']
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
