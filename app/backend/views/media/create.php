<?php

use yii\helpers\Html;



use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\media\MediaCategories;
/* @var $this yii\web\View */
/* @var $model common\models\media\Media */
/* @var $form yii\widgets\ActiveForm */

/* @var $this yii\web\View */
/* @var $model common\models\media\Media */

$this->title = Yii::t('events', 'Create Media');
$this->params['breadcrumbs'][] = ['label' => Yii::t('events', 'Media'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="media-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?></p>



<div class="media-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>


    <div class="row">
        <div class="col-6">
            <div class="card">
                <h4 class="card-header"><?= Yii::t('general', 'Panel Content') ?></h4>
                <div class="card-body">
                     <?= $form->field($model, 'title')->textInput() ?>
                    <?= $form->field($model, 'categories_id')->dropDownList(ArrayHelper::map(MediaCategories::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'select category')]); ?>
                  
                    <?= $form->field($model, 'imageFile')->fileInput() ?>
                </div>
                <div class="card-footer">
                    <?= Html::submitButton( Yii::t('articles', 'Create'), ['btn btn-success']) ?>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <h4 class="card-header"><?= Yii::t('general', 'Panel Content') ?></h4>
                <div class="card-body">
                    
                </div>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>

</div>
