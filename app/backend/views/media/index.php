<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\helpers\ArrayHelper;
use common\models\media\MediaCategories;

use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\media\MediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('events', 'Media');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('media', 'Create Media'), ['create'], ['class' => 'btn btn-success']) ?>
         <?= Html::a(Yii::t('media', 'Media Categories'), ['media-categories/index'], ['class' => 'btn btn-dark']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
            'title',
                [
                'attribute' => 'categories',
                'value' => 'categories.title',
                'filter' => ArrayHelper::map(MediaCategories::find()->asArray()->all(), 'id', 'title'),
            ],
            'name',
            
            [
                'attribute' => 'size',
                  'contentOptions' => ['style' => 'width: 145px;'],
               
            ],
             [
                'attribute' => 'url',
                  'contentOptions' => ['style' => 'width: 145px;'],
                'value' => function($model){
                return Html::img('/upload/thumb/small/'.$model->name,['class'=>'img-thumbnail']);
                },
                 'format' => 'raw'
                //'filter' => ArrayHelper::map(MediaCategories::find()->asArray()->all(), 'id', 'title'),
            ],
           
            //'thumbnailUrl',
            //'type',

              ['class' => 'common\hooks\yii2\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
