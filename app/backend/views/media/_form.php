<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\media\MediaCategories;
/* @var $this yii\web\View */
/* @var $model common\models\media\Media */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="media-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>


    <div class="row">
        <div class="col-6">
            <div class="card">
                <h4 class="card-header"><?= Yii::t('general', 'Panel Content') ?></h4>
                <div class="card-body">
                    <?= $form->field($model, 'categories_id')->dropDownList(ArrayHelper::map(MediaCategories::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'select category')]); ?>
                  
                    <?= $form->field($model, 'imageFile')->fileInput() ?>
                </div>
                <div class="card-footer">
                    <?= Html::submitButton( Yii::t('articles', 'Create'), ['btn btn-success']) ?>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <h4 class="card-header"><?= Yii::t('general', 'Panel Content') ?></h4>
                <div class="card-body">
                    
                </div>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
