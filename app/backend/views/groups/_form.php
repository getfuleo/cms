<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\media\MediaCategories;
use yii\helpers\ArrayHelper;
use common\models\teacher\Teacher;
use common\models\media\Media;

/* @var $this yii\web\View */
/* @var $model common\models\groups\Groups */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-8">
        <div class="card">
            <h4 class="card-header"><?= Yii::t('general', 'Panel Content') ?></h4>
            <div class="card-body">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

                <div class="row">
                    <div class="col-6"><?= $form->field($model, 'txt_1')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-6"><?= $form->field($model, 'txt_2')->textInput(['maxlength' => true]) ?></div>
                </div>
                <div class="row">
                    <div class="col-6"><?= $form->field($model, 'txt_3')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-6"><?= $form->field($model, 'txt_4')->textInput(['maxlength' => true]) ?></div>

                </div>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('articles', 'Create') : Yii::t('articles', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>

    </div>
    <div class="col-4">
        <div class="card">
            <h4 class="card-header"><?= Yii::t('general', 'Panel Settings') ?></h4>
            <div class="card-body">
                <?= $form->field($model, 'gallery_id')->dropDownList(ArrayHelper::map(MediaCategories::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'select category')]); ?>
                <?= $form->field($model, 'gallery_public')->dropDownList(ArrayHelper::map(MediaCategories::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'select category')]); ?>
                <?= $form->field($model, 'teacher')->dropDownList(ArrayHelper::map(Teacher::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'select teacher')]); ?>
                <?= $form->field($model, 'teacher_second')->dropDownList(ArrayHelper::map(Teacher::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'select teacher')]); ?>

            </div>
        </div>
        <div class="card mt-3">
            <h4 class="card-header"><?= Yii::t('general', 'Thumbnail') ?></h4>
            <div class="card-body">
                <?= $form->field($model, 'image')->dropDownList(ArrayHelper::map(Media::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'select category'), 'data-action' => 'image']); ?>
                <div data-item="image-ajax">
                    <?php if ($model->image) { ?>
                        <img src="/upload/<?= $model->getMedia()->one()->name ?>" class="img-thumbnail"/>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>