<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\groups\Groups */

$this->title = Yii::t('groups', 'Create Groups');
$this->params['breadcrumbs'][] = ['label' => Yii::t('groups', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groups-create">

    <h1><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?> <?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
