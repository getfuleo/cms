<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\events\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('events', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events">
    <div class="row">

        <!--        <div class="col-12 events-year">
                    <span class="badge badge-pill badge-primary">2017</span>
                    <span class="badge badge-pill badge-secondary">2018</span>
                    <span class="badge badge-pill badge-secondary">2019</span>
                </div>-->

        <div class="col-2 events-month">
            <!--            <ul>
                            <li><a href="#" title="#">Styczeń</a></li>
                            <li><a href="#" title="#">Luty</a></li>
                            <li><a href="#" title="#">Marzec</a></li>
                            <li><a href="#" title="#">Kwiecień</a></li>
                            <li><a href="#" title="#">Maj</a></li>
                            <li><a href="#" title="#">Czerwiec</a></li>
                            <li><a href="#" title="#">Lipiec</a></li>
                            <li><a href="#" title="#">Sierpień</a></li>
                            <li><a href="#" title="#">Wrzesień</a></li>
                            <li><a href="#" title="#">Październik</a></li>
                            <li><a href="#" title="#">Listopad</a></li>
                            <li><a href="#" title="#">Grudzień</a></li>
                        </ul>-->
            <h1><?= Yii::t('general', 'Search') ?></h1>
            <?= $this->render('_search', ['model' => $searchModel]) ?>
        </div>
        <div class="col-10 events-content">


            <h1><?= Html::encode($this->title) ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

            <p>
                <?= Html::a(Yii::t('events', 'Create Events'), ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a(Yii::t('events', 'Events Categories'), ['events-categories/index'], ['class' => 'btn btn-dark']) ?>
            </p>



            <?php Pjax::begin(); ?>    <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'pager' => [
                    'class' => 'common\hooks\yii2\widgets\LinkPager'
                ],
                'rowOptions' => function($model) {
                    if (!$model->active) {
                        return ['class' => 'table-danger'];
                    }
                },
                'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                    'title',
                        [
                        'attribute' => 'description_short',
                        'filter' => 'strip_tags',
                        'value' => function ($data) {
                            return strip_tags(html_entity_decode($data->description_short));
                        }
                    ],
                    'date_at',
                    'date_to',
                    // 'date_update',
                    // 'user_create',
                    // 'user_update',
                     'active',
                    // 'categories_id',
                    // 'date',
                    // 'slug',
                    ['class' => 'common\hooks\yii2\grid\ActionColumn']
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>

        </div>


    </div>

</div>
