<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\events\Events */

$this->title = Yii::t('events', 'Create Events');
$this->params['breadcrumbs'][] = ['label' => Yii::t('events', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-create">

    <h1><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?> <?= Html::encode($this->title) ?></h1>
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
