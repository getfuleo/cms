<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\events\Events */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('events', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('events', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::a(Yii::t('events', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?=
        Html::a(Yii::t('events', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('events', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
                [
                'attribute' => 'description',
                'format' => 'raw'
            ],
                [
                'attribute' => 'description_short',
                'format' => 'raw'
            ],
            'date_create',
            'date_update',
            'user_create',
            'user_update',
            'active',
            'categories_id',
            'date_at',
            'date_to',
            'slug',
        ],
    ])
    ?>

</div>
