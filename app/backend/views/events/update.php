<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\events\Events */

$this->title = Yii::t('events', 'Update {modelClass}: ', [
            'modelClass' => 'Events',
        ]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('events', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('events', 'Update');
?>
<div class="events-update">

    <h1><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?> <?= Html::encode($this->title) ?></h1>
   
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
