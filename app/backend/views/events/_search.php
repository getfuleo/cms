<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\events\EventsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <?= $form->field($model, 'year')->dropDownList([
        2017=>"2017",
        2018=>"2018",
        2019=>"2019",
        2020=>"2020",
        2021=>"2021"
       
        ],['prompt'=>'Wybierz rok']) ?>
    
    <?= $form->field($model, 'month')->dropDownList([
        1=>"Styczeń",
        2=>"Luty",
        3=>"Marzec",
        4=>"Kwiecień",
        5=>"Maj",
        6=>"Czerwiec",
        7=>"Lipiec",
        8=>"Siepień",
        9=>"Wrzesień",
        10=>"Październik",
        11=>"Listopad",
        12=>"Grudzień",
        ],['prompt'=>'Wybierz miesiąc']) ?>


    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'description_short') ?>



    <?php // echo $form->field($model, 'date_update') ?>

    <?php // echo $form->field($model, 'user_create') ?>

    <?php // echo $form->field($model, 'user_update') ?>

    <?php // echo $form->field($model, 'active') ?>

    <?php // echo $form->field($model, 'categories_id') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('events', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('events', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
