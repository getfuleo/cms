<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">

    <?php
    $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => [
                    'class' => 'form-signin'
                ]
    ]);
    ?>
    <h2 class="form-signin-heading"> <?= Html::encode($this->title) ?></h2>
    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox() ?>

    <div class="form-group">
<?= Html::submitButton('Login', ['class' => 'btn btn-lg btn-primary btn-block', 'name' => 'login-button']) ?>
    </div>



<?php ActiveForm::end(); ?>








