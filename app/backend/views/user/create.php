<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\groups\Groups;
use common\models\media\Media;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = Yii::t('user', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-8">
            <div class="card">
                <h4 class="card-header"><?= Yii::t('general', 'Panel Content') ?></h4>
                <div class="card-body">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    <div class="row">
                        <div class="col-6"><?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?></div>

                    </div>


                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('articles', 'Create'), ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>

        </div>
       
    </div>
    <?php ActiveForm::end(); ?>



</div>
