<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\articles\ArticlesCategories;
/* @var $this yii\web\View */
/* @var $searchModel common\models\articles\ArticlesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('articles', 'Articles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('articles', 'Create Articles'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('articles', 'Articles Categories'), ['articles-categories/index'], ['class' => 'btn btn-dark']) ?>
    </p>
    <?php // Pjax::begin(); ?> 
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'class' => 'common\hooks\yii2\widgets\LinkPager'
        ],
        'rowOptions' => function($model) {
            if (!$model->active) {
                return ['class' => 'table-danger'];
            }
        },
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
            'title',
                [
                'attribute' => 'categories_id',
                'value' => 'categories.title',
                'filter' => ArrayHelper::map(ArticlesCategories::find()->asArray()->all(), 'id', 'title'),
            ],
            // 'active',
            'date_create',
            'date_update',
            'slug',
            // 'user_create',
            // 'user_update',
            ['class' => 'common\hooks\yii2\grid\ActionColumn'],
        ],
    ]);
         
    ?>
    <?php // Pjax::end(); ?>
</div>
