<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\articles\ArticlesCategories;
use common\models\media\Media;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\articles\Articles */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$form = ActiveForm::begin();
?>
<div class="row">
    <div class="col-8">
        <div class="card">
            <h4 class="card-header"><?= Yii::t('general', 'Panel Content') ?></h4>
            <div class="card-body">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'description_short')->textarea(['rows' => 6]) ?>
                <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('articles', 'Create') : Yii::t('articles', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>

    </div>
    <div class="col-4">
        <div class="card">
            <h4 class="card-header"><?= Yii::t('general', 'Panel Settings') ?></h4>
            <div class="card-body">
                <?= $form->field($model, 'categories_id')->dropDownList(ArrayHelper::map(ArticlesCategories::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'select category')]); ?>
                <?= $form->field($model, 'active')->dropDownList([1 => Yii::t('general', 'active'), 0 => Yii::t('general', 'inactive')]) ?>
                <?= $form->field($model, 'slug')->textInput(['disabled' => true]) ?>
                <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="card mt-3">
            <h4 class="card-header"><?= Yii::t('general', 'Thumbnail') ?></h4>
            <div class="card-body">
                <?= $form->field($model, 'image')->dropDownList(ArrayHelper::map(Media::find()->all(), 'id', 'title'), ["prompt" => Yii::t('general', 'select category'), 'data-action' => 'image']); ?>
                <div data-item="image-ajax">
                    <?php if ($model->image) { ?>
                        <img src="/upload/<?= $model->getMedia() ?>" class="img-thumbnail"/>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="card mt-3">
            <h4 class="card-header"><?= Yii::t('general', 'Data') ?> <span class="badge badge-primary float-right" data-action="add-input"><i class="fa fa-plus"></i></span></h4>
            <div class="card-body" data-item="input-data">
                <?php
                if ($model->data) {

                    foreach (unserialize($model->data) as $key => $value) {
                        echo $form->field($model, 'data[' . $key . ']')->textInput(['value' => $value])->label('txt: ' . $key . '<span class="badge badge-danger float-right" data-action="delete-input" data-id="' . $key . '"><i class="fa fa-times"></i></span>');
                    }
                } else {
                    echo $form->field($model, 'data[0]')->textInput();
                }
                ?>




            </div>
        </div>
    </div>




</div>



<?php ActiveForm::end(); ?>

<?php
$js = <<< JS
        $('[data-action="add-input"]').click(function(){
     last = $('[data-item="input-data"]').find(".form-group").last();
        index = last.index() + 1;
     clone = last.clone();
      clone.find('input').val('');
      clone.attr('class', 'form-group field-articles-data-'+index);
      clone.find('label').html('Txt: '+ index + '<span class="badge badge-danger float-right" data-action="delete-input" data-id="'+index+'"><i class="fa fa-times"></i></span>');
      clone.find('label').attr('for', 'articles-data-'+index);
      clone.find('input').attr('id','articles-data-'+index);
      clone.find('input').attr('name','Articles[data]['+index+']');
		
    last.after(clone);
        console.log(last.index());
   });
        
          $('[data-action="delete-input"]').click(function(){
        console.log('delete');
        $(this).closest('.form-group').remove();
       });
        
JS;

$this->registerJs($js);
