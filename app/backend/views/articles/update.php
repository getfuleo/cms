<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\articles\Articles */

$this->title = Yii::t('articles', 'Update Articles');
$this->params['breadcrumbs'][] = ['label' => Yii::t('articles', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('articles', 'Update Articles');
?>
<div class="articles-update">

    <h1><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?> <?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
       
    ]) ?>

</div>
