<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\classifieds\ClassifiedsCategories;

/* @var $this yii\web\View */
/* @var $model common\models\classifieds\ClassifiedsCategoriesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classifieds-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'categories_id')->dropDownList(ArrayHelper::map(ClassifiedsCategories::find()->asArray()->all(), 'id', 'title')); ?>
    

    <?= $form->field($model, 'description_short') ?>

    <?= $form->field($model, 'date_create') ?>

    <?php // echo $form->field($model, 'date_update') ?>

    <?php // echo $form->field($model, 'user_create') ?>

    <?php // echo $form->field($model, 'user_update') ?>

    <?php // echo $form->field($model, 'active') ?>

    <?php // echo $form->field($model, 'categories_id') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('classifieds', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('classifieds', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
