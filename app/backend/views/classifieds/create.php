<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\classifieds\Classifieds */

$this->title = Yii::t('classifieds', 'Create Classifieds');
$this->params['breadcrumbs'][] = ['label' => Yii::t('classifieds', 'Classifieds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classifieds-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a(Yii::t('general', 'Return'), ['index'], ['class' => 'btn btn-secondary']) ?></p>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
