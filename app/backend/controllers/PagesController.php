<?php

namespace backend\controllers;

use Yii;
use common\models\pages\{
    Pages,
    PagesSearch
};
use common\models\block\Block;
use \common\components\ArrayHelperComponent;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;

/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        // 'actions' => [ 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Pages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        $blocks = ArrayHelperComponent::arrayGroup(Block::find()->where(['site_id' => $id])->all(), 'row');



        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'blocks' => $blocks,
            ]);
        }
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Creates a new block model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateBlock($id) {
        $model = new Block();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update-block', 'id' => $model->id]);
        } else {
            return $this->render('block/block-create', [
                        'model' => $model,
                        'id' => $id
            ]);
        }
    }

    /**
     * Creates a new block model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdateBlock($id) {

        $model = $this->findModelBlock($id);
        $folder = $this->findModules($model->module_id)->folder;
        $directories = FileHelper::findFiles(Yii::getAlias('@frontend/views/' . $folder . '/block'), ['recursive' => FALSE]);

      

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update-block', 'id' => $model->id]);
        } else {
            return $this->render('block/block-update', [
            'model' => $model,
            'directories' => $directories
            ]);
        }
    }

    public function actionDeleteBlock($id) {
        $model = $this->findModelBlock($id);
        $siteId = $model->site_id;
        $model->delete();

        return $this->redirect(['update', 'id' => $siteId]);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelBlock($id) {
        if (($model = Block::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModules($id) {
        if (($model = \common\models\modules\Modules::findOne($id)) !== null) {
            return $model;
        } else {
            return false;
        }
    }

}
