<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/library.css',
        'css/style.css',
    ];
    public $js = [
        'js/library.js',
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
          //'yii\bootstrap\BootstrapAsset',
    ];

}
