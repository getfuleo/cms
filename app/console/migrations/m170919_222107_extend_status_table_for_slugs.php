<?php

use yii\db\Migration;

/**
 * Class m170919_222107_extend_status_table_for_slugs
 */
class m170919_222107_extend_status_table_for_slugs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m170919_222107_extend_status_table_for_slugs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170919_222107_extend_status_table_for_slugs cannot be reverted.\n";

        return false;
    }
    */
}
