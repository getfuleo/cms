<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/library.css',
        'css/style.css'
    ];
    public $js = [
        'js/library.js',
        'js/script.js',
    ];
    public $depends = [
       'yii\web\YiiAsset',
       // 'yii\bootstrap\BootstrapAsset',
    ];
    
    public function init()
{
    parent::init();
    // resetting BootstrapAsset to not load own css files
    \Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
        'css' => [],
        'js' => []
    ];
//     \Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
//        'css' => [],
//        'js' => []
//    ];
      \Yii::$app->assetManager->bundles['yii\web\BootstrapAsset'] = [
        'css' => [],
        'js' => []
    ];
    
   
}
}
