<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\groups\Groups */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="groups-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'gallery_id')->textInput() ?>

    <?= $form->field($model, 'txt_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'txt_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'txt_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'txt_4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'teacher')->textInput() ?>

    <?= $form->field($model, 'teacher_second')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('groups', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
