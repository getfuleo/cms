<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\groups\Groups */

$this->title = Yii::t('groups', 'Create Groups');
$this->params['breadcrumbs'][] = ['label' => Yii::t('groups', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groups-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
