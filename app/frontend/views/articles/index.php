<?php

use yii\widgets\ListView;
?>

<section id="index-actual" class="actual section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>
                    Aktualności
                </h2>
                <p>
                    Zobacz co działo się w naszym przedszkolu
                </p>
            </div>


            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => 'item/item',
                'pager' => [
                    'class' => 'common\hooks\yii2\widgets\LinkPager'
                ],
                // 'summary' => false,
                //'layout' => "{items}",
                'layout' => "{items}\n<div class='col-12 d-flex justify-content-center'>{pager}</div>",
                'itemOptions' => [
                    'tag' => false
                ],
                'options' => [
                    'class' => 'nav nav-tabs',
                    'role' => 'tablist',
                    'id' => false,
                    'tag' => false
                ]
            ]);
            ?>

        </div>
    </div>
</section>
