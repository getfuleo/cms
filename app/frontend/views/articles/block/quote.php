<aside class="quote">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3>
                    <?=$model->description_short?>
                </h3>
                <p class="quote-author">
                    ~ <?=$model->description?>
                </p>
            </div>
        </div>
    </div>
</aside>


