 
<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    <?= $model->title ?>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="contents section-white">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>
                    <?= $model->description_short ?>
                </p>
                <p>
                    <?= $model->description ?>
                </p>
            </div>
        </div>
    </div>
</section>

