<?php

use yii\widgets\ListView;
?>
<div class="col-12 col-md-8">
    <div class="gallery-parent">
        <div class="row">
            <div class="col-12 ">
                <h3>
                    Galeria
                </h3>
            </div>
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => 'item/onsite_one',
                'summary' => false,
                'layout' => "{items}",
                'itemOptions' => [
                    'tag' => false
                ],
                'options' => [
//                        'class' => 'nav nav-tabs',
//                        'role' => 'tablist',
                    'id' => false,
                    'tag' => false
                ]
            ]);
            ?>

        </div>
    </div>
</div>




