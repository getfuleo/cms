<?php

use yii\widgets\ListView;
?>

<section class="gallery">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>
                    Galeria
                </h2>
                <p>
                    Galeria naszych przedszkolaków
                </p>
            </div>
        </div>
        <!-- single item -->
        <div class="slick row">

            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => 'item/one',
                'summary' => false,
                'layout' => "{items}",
                    'itemOptions' => [
                        'tag' => false
                    ],
                    'options' => [
//                        'class' => 'nav nav-tabs',
//                        'role' => 'tablist',
                        'id' => false,
                        'tag' => false
                    ]
            ]);
            ?>



        </div>
        <div class="row">
            <div class="col-12 text-center">
                <a href="#" class="btn btn-outline-secondary">zobacz więcej</a>
            </div> 
        </div>
    </div>
</div>
</section>

