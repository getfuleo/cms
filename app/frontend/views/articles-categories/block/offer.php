<?php

use yii\widgets\ListView;
?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    Oferta 
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="offer section-white">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?= $model->description_short ?>
                <?= $model->description ?>
            </div>
            <div class="col-12">
                <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => 'item/item_offer',
                    'summary' => false,
                    // 'layout' => "{items}",
                    'layout' => "{items}",
                    'itemOptions' => [
                        'tag' => false
                    ],
                    'options' => [
                        'class' => 'nav nav-tabs',
                        'role' => 'tablist',
                        'id' => false,
                        'tag' => false
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
</section>




