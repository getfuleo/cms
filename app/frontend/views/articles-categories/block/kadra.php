<?php

use yii\widgets\ListView;
?>
<div class="people col-md-8">
    <div class="row">
        <div class="col-12">
            <h3>
                Kadra
            </h3>

            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => 'item/item_kadra',
                'summary' => false,
                // 'layout' => "{items}",
                'layout' => "{items}",
                'itemOptions' => [
                    'tag' => false
                ],
                'options' => [
                    'class' => 'nav nav-tabs',
                    'role' => 'tablist',
                    'id' => false,
                    'tag' => false
                ]
            ]);
            ?>
        </div>
    </div>     
</div>



