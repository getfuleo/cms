<?php
use yii\widgets\ListView;
?>
<section id="top-slider" class="slider">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">

                        <?=
                        ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => 'item/item_slider_1',
                            'summary' => false,
                            // 'layout' => "{items}",
                            'layout' => "{items}\n{pager}",
                            'itemOptions' => [
                                'tag' => false
                            ],
                            'options' => [
                                'class' => 'nav nav-tabs',
                                'role' => 'tablist',
                                'id' => false,
                                'tag' => false
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>





