<!-- single- person -->
<div class="single">
    <div class="row">
        <div class="col-12  col-lg-3 ">
            <div class="item">
                <img  src="assets/main/img/people-1.jpg" alt="kadra">
            </div>
        </div>
        <div class="col-12 col-lg-9">
            <h4>
                <?= $model->title ?>
            </h4>
            <span class="proffesion">
                <?= $model->description_short ?>
            </span>
            <span class="group-list group-1">
                Grupa: Misie
            </span>
            <p>
                <?= $model->description ?>
            </p>
        </div>
    </div>
</div>