<!-- single slide -->
<div class="carousel-item active">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-5 text-center text-lg-left">
                <div class="txt">
                    <h2 class="wavy">
                        <?= $model->title ?>
                    </h2>
                    <p>
                        <?= $model->description_short ?>
                    </p>
                    <a href="/artykul/<?= $model->slug ?>" class="btn btn-outline-secondary">
                        czytaj więcej
                    </a>
                </div>
            </div>
            <div class="col-lg-7 d-none d-lg-block">
                <div class="image">
                    <img  src="/img/example-slide-1.png" alt="slajd">
                </div>
            </div>
        </div>
    </div>
</div>