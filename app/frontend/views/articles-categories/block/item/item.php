<?php
use common\helpers\TextHelper;
?>
<div class="col-12 col-md-6 col-lg-3">
    <div class="item">
        <img src="/upload/thumb/medium/<?= $model->getMedia() ?>" alt="<?= $model->title ?>">
        <h3>
            <?= TextHelper::substr($model->title,0,30); ?>
        </h3>
        <span>
            czytaj więcej
            <img src="/img/white-arrow.png" alt="czytaj więcej">
        </span>
        <a href="artykul/<?= $model->slug ?>" title="<?= $model->title ?>"></a>
    </div>
</div>