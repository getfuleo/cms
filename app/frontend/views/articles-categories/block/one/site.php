<?php

use yii\widgets\ListView;
?>
<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    <?= $model->title ?>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="contents section-white">
    <div class="container">
        <div class="row">
            <div class="side-menu col-md-4">
                <ul>
                    <?php foreach ($models as $row) { ?>
                        <li <?= $row->slug == $slug[1] ? ' class="active"' : '' ?>>
                            <a href="/<?= $slug[0] ?>/<?= $row->slug ?>">
                                <?= $row->title ?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>

            <div class="about col-md-8">
                <div class="row">
                    <div class="col-12">
                        <?php if ($type == 'widget') { ?>
                        
                        <?php } else { ?>
                            <?= $model->description ?>
                        <?php } ?>

                    </div>
                </div>     
            </div>
        </div>
    </div>
</section>

