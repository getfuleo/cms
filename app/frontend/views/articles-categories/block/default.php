<?php
use yii\widgets\ListView;
?>

<section id="index-actual" class="actual section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>
                    Aktualności
                </h2>
                <p>
                    Zobacz co działo się w naszym przedszkolu
                </p>
            </div>
          
           
            <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => 'item/item',
                    'summary' => false,
                    'layout' => "{items}",
                    //'layout' => "{items}\n{pager}",
                    'itemOptions' => [
                        'tag' => false
                    ],
                    'options' => [
                        'class' => 'nav nav-tabs',
                        'role' => 'tablist',
                        'id' => false,
                        'tag' => false
                    ]
]);
?>
           
            <div class="col-12 text-center">
                <a href="/aktualnosci" title="Aktualności" class="btn btn-outline-black">czytaj więcej</a>
            </div> 
        </div>
    </div>
</section>



