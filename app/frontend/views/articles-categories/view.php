 
<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    <?= $model->title ?>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="contents section-white">
    <div class="container">
        <div class="row">
            <div class="side-menu col-md-4">
                <ul>
                    <?php foreach ($articles as $row) { ?>

                        <li>
                            <a href="/artykul/<?=$row->slug?>">
                                <?=$row->title?>
                            </a>
                        </li>
                    <?php } ?>



                </ul>
            </div>
            <div class="about col-md-8">
                <div class="row">
                    <div class="col-12">
                        <h2> <?= $model->title ?></h2>
                        <p>

                            <?= $model->description_short ?>
                        </p>
                        <p>
                            <?= $model->description ?>
                        </p>

                    </div>
                </div>     
            </div>
        </div>
    </div>
</section>

