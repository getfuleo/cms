<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="Innhouse.pl">
        <meta name="robots" content="index,fallow">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <link rel="shortcut icon" href="/img/favico.ico" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">
        <!--[if lt IE 9]>
            <script src="/js/hack.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php $this->beginBody() ?>
        <main>


            <header class="header">
                <div class="container">
                    <div class="row">
                       
                        <div class="col-12 text-right">
                            <a  href="/strefa-rodzica" title="Strefa rodzica" class="btn btn-danger">
                              Strefa rodzica
                            </a>
                            <a class="btn btn-phone" href="tel:22 747 00 70" title="zadzwoń">
                                <img class="img-fluid" src="/img/big-phone.png" alt="logo">
                            </a>
                        </div>
                        <div class="col-12 text-right">
                            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                                <a class="navbar-brand " href="/" titel="przejdź do strony głównej">
                                    <img class="img-fluid" src="/img/big-logo.png" alt="logo">
                                </a>
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                                    <ul class="navbar-nav">
                                        <li class="nav-item active">
                                            <a class="nav-link  d-none d-lg-block" href="/">
                                                <i class="icon i-home"></i>
                                            </a>
                                            <a class="nav-link d-lg-none" href="/">
                                                strona główna
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/oferta">oferta</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/wydarzenia">wydarzenia</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/ogloszenia">ogłoszenia</a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="/informacje-dla-rodzica" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                informacje dla rodzica
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                <a class="dropdown-item" href="/informacje-dla-rodzica/adaptacje">Adaptacja</a>
                                                <a class="dropdown-item" href="/informacje-dla-rodzica/dni-wolne">Dni wolne</a>
                                            </div>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="/o-nas" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                O nas
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                <a class="dropdown-item" href="/o-nas/o-przedszkolu">O przedszkolu</a>
                                                <a class="dropdown-item" href="/o-nas/misja-przedszkola">Misja przedszkola</a>
                                                <a class="dropdown-item" href="/o-nas/galeria">Galeria</a>
                                                <a class="dropdown-item" href="/o-nas/kadra">Kadra</a>
                                                <a class="dropdown-item" href="/o-nas/kontakt">Kontakt</a>
                                            </div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/o-nas/kontakt">kontakt</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>

            <?php
//            NavBar::begin([
//                'brandLabel' => 'My Company',
//                'brandUrl' => Yii::$app->homeUrl,
//                'options' => [
//                    'class' => 'navbar-inverse',
//                ],
//            ]);
//            $menuItems = [
//                    ['label' => 'Home', 'url' => ['/site/index']],
//                    ['label' => 'About', 'url' => ['/site/about']],
//                    ['label' => 'Contact', 'url' => ['/site/contact']],
//            ];
//            if (Yii::$app->user->isGuest) {
//                $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
//                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
//            } else {
//                $menuItems[] = '<li>'
//                        . Html::beginForm(['/site/logout'], 'post')
//                        . Html::submitButton(
//                                'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout']
//                        )
//                        . Html::endForm()
//                        . '</li>';
//            }
//            echo Nav::widget([
//                'options' => ['class' => 'navbar-nav navbar-right'],
//                'items' => $menuItems,
//            ]);
//            NavBar::end();
            ?>

          
                 <?= $content ?>
         
           


            <footer class="footer section-white">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6  col-lg-2 text-center text-lg-left">
                            <div class="footer-combine">
                                <ul class="text-uppercase">
                                    <li>
                                        <a href="/">
                                            STRONA GŁÓWNA
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/oferta">
                                            OFERTA
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/wydarzenia">
                                            WYDARZENIA
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/ogloszenia">
                                            OGŁOSZENIA
                                        </a>
                                    </li>
                                    <li>
                                        <a href="informacje-dla-rodzica">
                                            INFORMACJA DLA RODZICA
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/grupy">
                                            GRUPY
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-2 text-center text-lg-left">
                            <div class="footer-combine">
                                <ul class="text-uppercase">
                                    <li class="active">
                                        <a href="about.php">
                                            O PRZEDSZKOLU
                                        </a>
                                    </li>
                                    <li>
                                        <a href="about.php">
                                            O NAS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="about.php">
                                            INFORMACJA DLA RODZICA
                                        </a>
                                    </li>
                                    <li>
                                        <a href="people.php">
                                            KADRY
                                        </a>
                                    </li>
                                    <li>
                                        <a href="contact.php">
                                            KONTAKT
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6  col-lg-1 no-padding text-center  text-lg-left">
                            <div class="footer-combine">
                                <img class="img-fluid" src="/img/logo.png" alt="logo">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-4 col-xl-3 text-center text-lg-left">
                            <div class="footer-combine">
                                <p>
                                    <b>
                                        NIEPUBLICZNE PRZEDSZKOLE CYPISEK
                                    </b>
                                    <br>
                                    UL. OSTRÓDZKA 213K,
                                    <br>
                                    03-289 WARSZAWA
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-2 no-padding text-center  text-lg-left">
                            <div class="footer-combine">
                                <p>
                                    <b>
                                        GODZINY OTWARCIA:
                                    </b>
                                    <br>
                                    PONIEDZIAŁEK-PIĄTEK
                                    <br>
                                    6:30 - 18:00
                                    <br>
                                    <b>
                                        TELEFONY KONTAKTOWE:
                                    </b>
                                    <br>
                                    <a href="tel:22 747 00 70" title="zadzwoń">
                                        22 747 00 70
                                    </a>
                                    <br>
                                    <a href="tel:22 747 00 70" title="zadzwoń">
                                        0 606 202 215
                                    </a>
                                    <br>
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-1 text-center text-lg-left">
                            <div class="footer-combine">
                                <a href="#" title="przejdź do facebooka">
                                    <img class="img-fluid" src="/img/fb.png" alt="ikony">
                                </a>
                            </div>
                        </div>
                        <div class="col-12  text-center">
                            <div class="footer-combine">
                                <div class="logo-bot">
                                    <b>
                                        projekt i wykonanie:
                                    </b>
                                    <img class="img-fluid" src="/img/innohouse.png" alt="logo">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </footer>
        </main> 
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
