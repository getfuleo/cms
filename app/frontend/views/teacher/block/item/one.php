<div class="single">
    <div class="row">
        <div class="col-12  col-lg-3 ">
            <div class="item">
                <img src="/upload/thumb/small/<?=  $model->getMedia() ? $model->getMedia()->one()->name : '' ?>" alt=" <?= $model->title ?>">
            </div>
        </div>
        <div class="col-12 col-lg-9">
            <h4>
                <?= $model->title ?>
            </h4>
            <span class="proffesion">
                <?= $model->subtitle ?>
            </span>
            <?php if($model->getGroups()->one()){ ?>
            <span class="group-list <?= $model->getGroups()->one()->txt_4 ?>">
                Grupa: <?= $model->getGroups()->one() ? $model->getGroups()->one()->title : ''?>
            </span>
            <?php } ?>
            <p>
                <?= $model->description ?>
            </p>
        </div>
    </div>
</div>

