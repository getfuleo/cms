<?php

use yii\helpers\Html;
use yii\widgets\ListView;

$group = Yii::$app->user->identity->getGroups()->one();
?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8">
                <h1 class="wavy">
                    Strefa rodzica 
                </h1>
            </div>
            <div class="col-12 col-sm-4 text-left text-sm-right">


                <?= Html::a('<i class="icon i-off"></i>wyloguj się', ['site/logout'], ['data' => ['method' => 'post'], 'class' => 'btn btn-light']) ?>

            </div>
        </div>
    </div>
</section>
<section class="parent-head section-white">
    <div class="container">
        <div class="row centered-row">
            <div class="col-md-2 d-none d-md-block">
                <div class="first">
                    <?php if($group->hasMedia()) { ?>
                    <img src="/upload/<?= $group->getMedia()->one()->name ?>" alt="<?= $group->title ?>">
                    <?php } ?>
                </div>
            </div>
            <div class="col-12 col-md-3 text-center text-lg-left ">
                <div class="second">
                    <h2>
                        Grupa <?= $group->title ?>
                    </h2>
                </div>
            </div>
            <div class="col-12 col-md-7 text-center text-md-left">
                <div class="third">
                    <?= $group->description ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="parent-page section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="main-infos">
                    <div class="infos infos-primary">
                        <div class="row">
                            <div class="col-12 col-lg-3 text-center">
                                <img class="img-fluid" src="/upload/<?= $group->getMedia()->one()->name ?>" alt="<?= $group->title ?>">
                            </div>
                            <div class="col-12 col-lg-4 text-center no-padding">
                                <span>
                                    <?= $group->txt_1 ?>
                                </span>
                                <p>
                                    <b>
                                        Ilość dzieci
                                    </b>
                                </p>
                            </div>
                            <div class="col-12 col-lg-5 text-center">
                                <span>
                                    <?= $group->txt_2 ?>
                                </span>
                                <p>
                                    <b>
                                        Wiek dzieci
                                    </b>
                                </p>
                            </div>
                        </div>
                    </div>
                     <?php if( $group->getTeacher()->one() ){ ?>
                    <div class="infos infos-secondary">
                        <div class="row">
                            <div class="col-12 col-lg-4 text-center text-lg-left">
                                <div class="item">
                                    
                                     <?php if($group->getTeacher()->one()->hasMedia()) { ?>
                                    <img src="/upload/<?= $group->getTeacher()->one()->getMedia()->one()->name ?>" alt="osoba">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-12 col-lg-8 no-padding text-center text-lg-left">
                                <h4>
                                    <?= $group->getTeacher() ? $group->getTeacher()->one()->title : '' ?>
                                </h4>
                                <p>
                                    <strong>
                                        <?= $group->getTeacher()->one()->subtitle ?>
                                    </strong>
                                </p>
                            </div>
                        </div>
                    </div>
                     <?php }?>
              <?php if( $group->getTeacherSecond()->one() ){ ?>
                    <div class="infos infos-secondary">
                        <div class="row">
                            <div class="col-12 col-lg-4 text-center text-lg-left">
                                <div class="item">
                                    <img src="/upload/<?= $group->getTeacherSecond()->one()->getMedia()->one()->name ?>" alt="<?= $group->getTeacherSecond()->one()->title ?>">
                                </div>
                            </div>
                            <div class="col-12 col-lg-8 no-padding text-center text-lg-left">
                                <h4>
                                    <?= $group->getTeacherSecond()->one()->title ?>
                                </h4>
                                <p>
                                    <strong>
                                        <?= $group->getTeacherSecond()->one()->subtitle ?>
                                    </strong>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="infos infos-primary">
                        <div class="row">
                            <div class="col-12 col-lg-5 text-center text-lg-left">
                                <p>
                                    <b>
                                        Sekretariat
                                    </b>
                                <p>
                            </div>
                            <div class="col-12 col-lg-7 text-center text-lg-left">
                                <b>
                                    <a href="tel:22 747 00 70">
                                        22 747 00 70
                                    </a>
                                </b>  
                                <i class="icon i-phone ">
                                </i>
                            </div>
                        </div>
                    </div>
                    <div class="infos infos-primary">
                        <div class="row">
                            <div class="col-12 text-center text-lg-left">
                                <p>
                                    <b>
                                        <?= $group->txt_3 ?>
                                    </b>
                                <p>
                            </div>
                        </div>
                    </div>
                    <div class="infos infos-primary">
                        <div class="row">
                            <div class="col-12 text-center text-lg-left">
                                <p>
                                    <b>
                                        NIEPUBLICZNE PRZEDSZKOLE CYPISEK
                                    </b>
                                    <br>
                                    UL. OSTRÓDZKA 213K,
                                    <br>
                                    03-289 WARSZAWA
                                    <br>
                                    <br>
                                    <b>
                                        GODZINY OTWARCIA:
                                    </b>
                                    <br>
                                    PONIEDZIAŁEK-PIĄTEK
                                    <br>
                                    6:30 - 18:00
                                    <br>
                                    <br>
                                    <br>
                                    <b>
                                        TELEFONY KONTAKTOWE:
                                    </b>
                                    <br>
                                    <a href="tel:22 747 00 70" title="zadzwoń">
                                        22 747 00 70
                                    </a>
                                    <br>
                                    <a href="tel:22 747 00 70" title="zadzwoń">
                                        0 606 202 215
                                    </a>
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8">
                <div class="gallery-parent">
                    <div class="row">
                        <div class="col-12 ">
                            <h3>
                                Galeria
                            </h3>
                        </div>


                        <?php foreach ($group->getGallery()->one()->getMedia()->all() as $value) { ?>
                            <div class="col-12 col-sm-6 col-lg-3 ">
                                <div class="item">
                                    <img src="/upload/<?= $value->name ?>" alt="galeria">
                                    <a  href="/upload/<?= $value->name ?>" data-lightbox="roadtrip"></a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>