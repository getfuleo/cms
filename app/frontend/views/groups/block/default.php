<?php

use yii\widgets\ListView;
?>
<section class="groups section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>
                    Grupy
                </h2>
                <p>
                    Poznaj naszych przedszkolaków
                </p>
            </div>

            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => 'item/one',
                'summary' => false,
                'layout' => "{items}",
                'itemOptions' => [
                    'tag' => false
                ],
                'options' => [
                    'tag' => false
                ]
            ]);
            ?>
          
        </div>
    </div>
</section>