<?php
use yii\widgets\ListView;
?>

<section class="wall section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="side">
                    <div class="text-center">
                        <h2>
                            Wydarzenia
                        </h2>
                        <p>
                            Wydarzenia, które odbędą się w naszym przedszkolu
                        </p>
                    </div>
                    <?=
                    ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => 'item/one',
                        'summary' => false,
                        'layout' => "{items}",
//                    'itemOptions' => [
//                        'tag' => false
//                    ],
//                    'options' => [
//                        'class' => 'nav nav-tabs',
//                        'role' => 'tablist',
//                        'id' => false,
//                        'tag' => 'ul'
//                    ]
                    ]);
                    ?>
                  
                    
                   
                    <div class="text-center text-lg-left">
                        <a href="/wydarzenia" class="btn btn-outline-success">więcej wydarzeń</a>
                    </div>
                </div>
            </div>



