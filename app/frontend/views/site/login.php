<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Strefa rodzica';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    <?= Html::encode($this->title) ?>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="login-page section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8 col-md-6 col-lg-4 text-center margin-auto">
                <div class="login-content">
                    <h3>
                        Panel logowania
                    </h3>
                    <p>
                        Dostęp do strefy rodzica możliwy jest po zalogowaniu się. Dane do logowania można uzyskać w sekretariacie przedszkola.
                    </p>

                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>


                    <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => "Wpisz login"]) ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <div class="form-group">
                        <?= Html::submitButton('Zaloguj się', ['class' => 'btn btn-success btn-submit', 'name' => 'login-button']) ?>
                    </div>


                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
