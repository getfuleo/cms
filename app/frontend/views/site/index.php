<?php

/* @var $this yii\web\View */

$this->title = 'Cypisek - Przedszkole Niepubliczne - Warszawa Białołęka';
?><section id="top-slider" class="slider">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <!-- single slide -->
                        <div class="carousel-item active">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-lg-5 text-center text-lg-left">
                                        <div class="txt">
                                            <h2 class="wavy">
                                                Zapisy na rok szkolny<br>
                                                2017/2018
                                            </h2>
                                            <p>
                                                Poznaj nasze przedszkole. Zapoznaj się z naszą ofertą.  
                                            </p>
                                            <a href="/offer.php" class="btn btn-outline-secondary">
                                                czytaj więcej
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 d-none d-lg-block">
                                        <div class="image">
                                            <img  src="/img/example-slide-1.png" alt="slajd">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- single slide -->
                        <div class="carousel-item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-lg-5 text-center text-lg-left">
                                        <div class="txt">
                                            <h2 class="wavy">
                                                Zapisy na rok szkolny<br>
                                                2017/2018
                                            </h2>
                                            <p>
                                                Poznaj nasze przedszkole. Zapoznaj się z naszą ofertą.  
                                            </p>
                                            <a href="/offer.php" class="btn btn-outline-secondary">
                                                czytaj więcej
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 d-none d-lg-block">
                                        <div class="image">
                                            <img src="/img/example-slide-1.png" alt="slajd">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="index-actual" class="actual section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>
                    Aktualności
                </h2>
                <p>
                    Zobacz co działo się w naszym przedszkolu
                </p>
            </div>
            
            
            
            <?php foreach($articles as $row){ ?>
            
             <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img src="/img/example-gallery-4.jpg" alt="aktualność">
                    <h3>
                       <?= $row->title ?>
                    </h3>
                    <span>
                        czytaj więcej
                        <img src="/img/white-arrow.png" alt="strzałka">
                    </span>
                    <a href="/artykul/<?=$row->slug?>" title="<?= $row->title ?>"></a>
                </div>
            </div>
            <?php } ?>
           
            
            
            
            
            <div class="col-12 text-center">
                <a href="#" class="btn btn-outline-black">czytaj więcej</a>
            </div> 
        </div>
    </div>
</section>

<aside class="quote">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3>
                    Dziecko może nauczyć dorosłych trzech rzeczy: cieszyć się bez powodu, 
                    być ciągle czymś zajętym i domagać się ze wszystkich sił tego, czego pragnie.  
                </h3>
                <p class="quote-author">
                    ~Paulo Coelho
                </p>
            </div>
        </div>
    </div>
</aside>
<section class="wall section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="side">
                    <div class="text-center">
                        <h2>
                            Wydarzenia
                        </h2>
                        <p>
                            Wydarzenia, które odbędą się w naszym przedszkolu
                        </p>
                    </div>
                    <div class="item">
                        <div class="col-xs-12 no-padding">
                            <h3>
                                Spotkanie z policjantami
                            </h3>
                            <p>
                                W lipcu w naszym przedszkolu odbędzię się spotkanie z grupą 
                                policjantów, którzy nauczą dzieci zasad bezpieczeństwa
                                na drodzę oraz w miejscach publicznych.
                            </p>
                        </div>
                        <span>
                            10 lipiec 2017
                        </span>
                        <img src="assets/main/img/arrow.png" alt="galeria">
                        <a href="#" title="Wydarzenie"></a>
                    </div>
                    <div class="item">
                        <div class="col-xs-12 no-padding">
                            <h3>
                                Spotkanie z policjantami
                            </h3>
                            <p>
                                W lipcu w naszym przedszkolu odbędzię się spotkanie z grupą 
                                policjantów, którzy nauczą dzieci zasad bezpieczeństwa
                                na drodzę oraz w miejscach publicznych.
                            </p>
                        </div>
                        <span>
                            10 lipiec 2017
                        </span>
                        <img src="assets/main/img/arrow.png" alt="galeria">
                        <a href="#" title="Wydarzenie"></a>
                    </div>
                    <div class="item">
                        <div class="col-xs-12 no-padding">
                            <h3>
                                Spotkanie z policjantami
                            </h3>
                            <p>
                                W lipcu w naszym przedszkolu odbędzię się spotkanie z grupą 
                                policjantów, którzy nauczą dzieci zasad bezpieczeństwa
                                na drodzę oraz w miejscach publicznych.
                            </p>
                        </div>
                        <span>
                            10 lipiec 2017
                        </span>
                        <img src="assets/main/img/arrow.png" alt="galeria">
                        <a href="#" title="Wydarzenie"></a>
                    </div>
                    <div class="text-center text-lg-left">
                        <a href="#" class="btn btn-outline-success">więcej wydarzeń</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-6">

                <div class="side">
                    <div class="text-center">
                        <h2>
                            Ogłoszenia
                        </h2>
                        <p>
                            Wydarzenia, które odbędą się w naszym przedszkolu
                        </p>
                    </div>
                    <div class="item">
                        <div class="col-xs-12 no-padding">
                            <h3>
                                Spotkanie z policjantami
                            </h3>
                            <p>
                                W lipcu w naszym przedszkolu odbędzię się spotkanie z grupą 
                                policjantów, którzy nauczą dzieci zasad bezpieczeństwa
                                na drodzę oraz w miejscach publicznych.
                            </p>
                        </div>
                        <span>
                            10 lipiec 2017
                        </span>
                        <img src="assets/main/img/arrow.png" alt="galeria">
                        <a href="#" title="Ogłoszenie"></a>
                    </div>
                    <div class="item">
                        <div class="col-xs-12 no-padding">
                            <h3>
                                Spotkanie z policjantami
                            </h3>
                            <p>
                                W lipcu w naszym przedszkolu odbędzię się spotkanie z grupą 
                                policjantów, którzy nauczą dzieci zasad bezpieczeństwa
                                na drodzę oraz w miejscach publicznych.
                            </p>
                        </div>
                        <span>
                            10 lipiec 2017
                        </span>
                        <img src="assets/main/img/arrow.png" alt="galeria">
                        <a href="#" title="Ogłoszenie"></a>
                    </div>
                    <div class="item">
                        <div class="col-xs-12 no-padding">
                            <h3>
                                Spotkanie z policjantami
                            </h3>
                            <p>
                                W lipcu w naszym przedszkolu odbędzię się spotkanie z grupą 
                                policjantów, którzy nauczą dzieci zasad bezpieczeństwa
                                na drodzę oraz w miejscach publicznych.
                            </p>
                        </div>
                        <span>
                            10 lipiec 2017
                        </span>
                        <img src="assets/main/img/arrow.png" alt="galeria">
                        <a href="#" title="Ogłoszenie"></a>
                    </div>
                    <div class="text-center text-md-right">
                        <a href="#" class="btn btn-outline-success">więcej ogłoszeń</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="gallery">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>
                    Galeria
                </h2>
                <p>
                    Galeria naszych przedszkolaków
                </p>
            </div>
        </div>
        <!-- single item -->
        <div class="slick row">
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img src="/img/example-gallery-1.jpg" alt="galeria">
                    <a  title="Galeria" href="/img/example-gallery-1.jpg" data-lightbox="roadtrip"></a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img src="/img/example-gallery-1.jpg" alt="galeria">
                    <a  title="Galeria" href="/img/example-gallery-1.jpg" data-lightbox="roadtrip"></a>
                </div>
            </div>
            <!-- single item -->
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img  src="/img/example-gallery-2.jpg" alt="galeria">
                    <a  title="Galeria" href="/img/example-gallery-2.jpg" data-lightbox="roadtrip"></a>
                </div>
            </div>
            <!-- single item -->
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img  src="/img/example-gallery-3.jpg" alt="galeria">
                    <a  title="Galeria" href="/img/example-gallery-3.jpg" data-lightbox="roadtrip"></a>
                </div>
            </div>
            <!-- single item -->
            <div class="col-12 col-md-6 col-lg-3">
                <div class="item">
                    <img  src="/img/example-gallery-4.jpg" alt="galeria">
                    <a  title="Galeria" href="/img/example-gallery-4.jpg" data-lightbox="roadtrip"></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <a href="#" class="btn btn-outline-secondary">czytaj więcej</a>
            </div> 
        </div>
    </div>
</div>
</section>
<section class="groups section-white">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>
                    Grupy
                </h2>
                <p>
                    Poznaj naszych przedszkolaków
                </p>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="item">
                    <img class="img-fluid" src="/img/group-1.png" alt="grupa">
                    <h3>
                        Pszczółki
                    </h3>
                    <a href="#" title="Grupa"></a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="item">
                    <img class="img-fluid" src="/img/group-2.png" alt="grupa">
                    <h3>
                        Biedronki
                    </h3>
                    <a href="#"  title="Grupa"></a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="item">
                    <img class="img-fluid" src="/img/group-3.png" alt="grupa">
                    <h3>
                        Myszki
                    </h3>
                    <a href="#"  title="Grupa"></a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="item">
                    <img class="img-fluid" src="/img/group-4.png" alt="grupa">
                    <h3>
                        Kotki
                    </h3>
                    <a href="#"  title="Grupa"></a>
                </div>
            </div>
            <div class="col-12 text-center">
                <a href="#" class="btn btn-outline-black">czytaj więcej</a>
            </div> 
        </div>
    </div>
</section>