<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

?>
<div class="container site-error">

    <h1>404 gdzie jest strona?</h1>

    <div class="alert alert-danger">
     Brak strony pod podanym adresem
    </div>

    <p>
       Proszę sprawdzić poprawność linku.
    </p>
    <p>
      W razie problemów proszę skontaktować się z właścicielem strony
    </p>

</div>
