<?php

use yii\widgets\ListView;
?>
<div class="col-12 col-lg-6">
    <div class="side">
        <div class="text-center">
            <h2>
                Ogłoszenia
            </h2>
            <p>
                Ogłoszenia z naszego przedszkola
            </p>
        </div>
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'item/one',
            'summary' => false,
            'layout' => "{items}",
//                    'itemOptions' => [
//                        'tag' => false
//                    ],
//                    'options' => [
//                        'class' => 'nav nav-tabs',
//                        'role' => 'tablist',
//                        'id' => false,
//                        'tag' => 'ul'
//                    ]
        ]);
        ?>
        <div class="text-center text-md-right">
            <a href="/ogloszenia" class="btn btn-outline-success">więcej ogłoszeń</a>
        </div>
    </div>
</div>
</div>
</div>
</section>