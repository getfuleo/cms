<?php
$formatter = \Yii::$app->formatter;
?>
<div class="item">
    <div class="col-xs-12 no-padding">
        <h3>
            <?= $model->title ?>
        </h3>
        <p>
            <?= $model->description ?>
        </p>
    </div>
    <span>
        <?= $formatter->asDate($model->date_create, 'php:Y-m-d') ?>
    </span>
    <img src="/img/arrow.png" alt="Zobacz więcej">
    <a href="/ogloszenia/<?= $model->slug ?>" title="<?= $model->title ?>"></a>
</div>