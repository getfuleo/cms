<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Ogłoszenia';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    Ogłoszenia
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="contents section-white">
    <div class="container">
        <div class="row">
            <div class="side-menu col-md-4 ">
                <div class="dropdown dropdowns-events">
                    <button class="btn btn-dropdown-events dropdown-toggle" type="button" data-toggle="dropdown"> <?= $year?>
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu first">
                        <li <?= $year == 2017 ? 'class="active"' : ''; ?>>
                            <a href="/ogloszenia/2017/styczen" title="Ogłoszenia - 2017">
                                2017
                            </a>
                        </li>
                        <li <?= $year == 2018 ? 'class="active"' : ''; ?>>
                            <a href="/ogloszenia/2018/styczen" title="Ogłoszenia - 2018">
                                2018
                            </a>
                        </li>
                    </ul>
                </div>
                
                <div class="dropdown dropdowns-events">
                    <button class="btn btn-dropdown-events dropdown-toggle" type="button" data-toggle="dropdown"><?=$month['id']?>. <?=$month['name']?>
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">

                        <?php foreach($months as $key => $value) { ?>
                            <li <?= $value['slug'] == $month['slug'] ? 'class="active"' : '' ?>>
                                <a href="/ogloszenia/<?= $year ?>/<?= $value['slug'] ?>">
                                    <?= $key+1 ?>. <?= $value['name'] ?>
                                </a>
                            </li>
                        <?php } ?>

                    </ul>
                </div>
            </div>
            <div class="events col-md-8">
                <div class="row">
                    <div class="col-12">
                        <h3>
                            <span>Ogłoszenia w przedszkolu Cypisek w miesiącu:</span> <?=$month['name']?> <?= $year?>
                        </h3> 

                        <?=
                        ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => 'one',
                            // 'layout' =>'' ,
                            'summary' => '',
                            'viewParams' => [
                                'fullView' => true,
                                'context' => 'main-page',
                            // ...
                            ],
                        ]);
                        ?>






                    </div>
                </div>
            </div>     

        </div>
    </div>
</section>
