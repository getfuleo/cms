<div class="single">
    <div class="row">
        <div class="col-6 col-md-2 ">
            <span class="id">
                <?=$index+1 ?>
            </span>
        </div>
        <div class="col-6 d-md-none text-right">
            <span class="date"> 
                <?= $model->date_show ? date("d.m", strtotime($model->date_at)) : '';?>
            </span>
        </div>
        <div class="col-12 col-md-8">
            <h4>
                <?=$model->title ?>
            </h4>
            <p>
               <?= strip_tags(html_entity_decode($model->description_short));?>
            </p>
        </div>
        <div class=" col-12 col-md-2 d-none d-md-block">
            <span class="date"> 
                
                
                <?= $model->date_show ? date("d.m", strtotime($model->date_at)) : '';?>
                
            </span>
        </div>
    </div>
</div>