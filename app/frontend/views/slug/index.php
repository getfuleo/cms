<?php

/* @var $this yii\web\View */

use common\widgets\Block;

$this->title = $page->title;

/*
 * Get [row] in Array
 */
foreach ($blocks as $row) {
    /*
     * Get [col] in Array from [row]
     */
    echo Block::widget([
        'data' => $row,
        'slug' => $slug,
    ]);
} 
