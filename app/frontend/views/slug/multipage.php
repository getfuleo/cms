<?php
/* @var $this yii\web\View */

use common\widgets\Block;
use yii\widgets\ListView;

$this->title = $page->title;
?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="wavy">
                    <?= $model->title ?>
                </h1>
            </div>
        </div>
    </div>
</section> 

<section class="contents section-white">
    <div class="container">
        <div class="row">
            <div class="side-menu col-md-4">
                <ul>
                    <?php foreach ($page->getBlock() as $row) { ?>
                        <li <?= $row['slug'] == $slug ? ' class="active"' : '' ?>>
                            <a href="/<?= $page->slug ?>/<?= $row['slug'] ?>">
                                <?= $row['title'] ?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <?=
            Block::widget([
                'slug' => $slug,
                'data' => $model
            ]);
            ?>
        </div>
    </div>
</section>




