<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

use common\models\events\Events;
/**
 * Site controller
 */
class EventsController extends Controller {

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($year, $month) {


        $months     = Events::months();
        $month      = Events::findMonth($month);
      

        $searchModel = new \common\models\events\EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(["month(date_at)" => $month['id']]);
        $dataProvider->query->andWhere(["year(date_at)" => $year]);
        $dataProvider->pagination = array(
            'pagesize' => 20,
        );

        $dataProvider->setSort([
            'defaultOrder' => [
                'date_at' => SORT_ASC
            ]
        ]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'year' => $year,
                    'month' => $month,
                    'months' => $months,
            
        ]);
    }

}
