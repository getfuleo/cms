<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

use common\models\classifieds\Classifieds;
/**
 * Site controller
 */
class ClassifiedsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return[];
    }
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($year=false, $month=false) {


        $months     = \common\models\events\Events::months();
        $month      = \common\models\events\Events::findMonth($month);
      

        $searchModel = new \common\models\classifieds\ClassifiedsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(["month(date_at)" => $month['id']]);
        $dataProvider->query->andWhere(["year(date_at)" => $year]);
        $dataProvider->pagination = array(
            'pagesize' => 20,
        );

        $dataProvider->setSort([
            'defaultOrder' => [
                'date_at' => SORT_ASC
            ]
        ]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'year' => $year,
                    'month' => $month,
                    'months' => $months,
            
        ]);
    }

}
