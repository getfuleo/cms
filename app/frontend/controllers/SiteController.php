<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\pages\Pages;
use common\models\block\Block;
use common\components\ArrayHelperComponent;
use yii\helpers\Url;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {

        return $this->render('index', [
                    'articles' => \common\models\articles\Articles::find()->limit(4)->orderBy(['date_create' => SORT_DESC])->all()
        ]);
    }

    public function actionSlug($slug) {

        if (Yii::$app->request->getUrl() == "/") {
            $slug = "/";
            $slugArray = false;
        } else {
            $slug = Yii::$app->request->getUrl() == "/" ? "/" : ltrim(rtrim(Yii::$app->request->getUrl(), '/'), '/');
            $slugArray = explode("/", $slug);
            $slug = $slugArray[0];
        }

        $page = Pages::find()->where(['slug' => $slug])->one();

        if ($page) {
            // $blocks = ArrayHelperComponent::arrayGroup(Block::find()->where(['site_id' => $page->id])->orderBy('row')->all(), 'row');
            // echo '<pre>';
            //($blocks).exit();
            if ($page->access == 1 && Yii::$app->user->isGuest) {
                return $this->redirect(Url::toRoute('login'));
            }
            if ($page->type == 'multipage') {

                //   $block = Block::findOne()->where(['site_id' => $page->id])->orderBy('row')->all();
                if (isset($slugArray[1])) {
                    $block = Block::findOne(['slug' => $slugArray[1]]);
                    if (!$block) {
                        return header('Location: /' . $page->slug) . exit();
                    }
                    $slug = $block->slug;
                } else {
                    $block = Block::findOne(['site_id' => $page->id]);
                    $slug = $block->slug;
                }

                return $this->render('//slug/multipage', [
                            'page' => $page,
                            'model' => $block,
                            'slug' => $slug
                ]);
            } else {
                $blocks = Block::find()->where(['site_id' => $page->id])->orderBy('row')->all();
                return $this->render('//slug/index', [
                            'page' => $page,
                            'blocks' => $blocks,
                            'slug' => $slugArray
                ]);
            }
        } else {
            return $this->render('error');
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/strefa-rodzica');
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionArticles($id = false) {
        echo"<pre>";

        \yii\helpers\VarDumper::dump($id);
        echo"</pre>";
        return $this->render('articles');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup() {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->signup();
        }

        return $this->render('signup', [
                    'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

}
