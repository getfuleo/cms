<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
              
                // '' => 'site/index',
                // 'wydarzenia/<id>' => 'site/events',
                [
                    'pattern' => 'login',
                    'route' => 'site/login',
                ],
                [
                    'pattern' => 'logout',
                    'route' => 'site/logout',
                ],
                [
                    'pattern' => 'signup',
                    'route' => 'site/signup',
                ],
                [
                    'pattern' => 'artykul/<slug>',
                    'route' => 'articles/view',
                    'defaults' => ['slug' => '/'],
                ],
                [
                    'pattern' => 'aktualnosci',
                    'route' => 'articles/index',
                //'defaults' => ['slug' => '/'],
                ],
                
                [
                    'pattern' => 'wydarzenia/<year:\d+>/<month>',
                    'route' => 'events/index',
                    'defaults' => ['year' => 2017, 'month' => 'styczen'],
                ],
                [
                    'pattern' => 'ogloszenia',
                    'route' => 'classifieds/index',
                    'defaults' => ['year' => 2017, 'month' => 'styczen'],
                ],
                [
                    'pattern' => 'ogloszenia/<year:\d+>/<month>',
                    'route' => 'classifieds/index',
                    'defaults' => ['year' => 2017, 'month' => 'styczen'],
                ],
                [
                    'pattern' => '<slug>/',
                    'route' => 'site/slug',
                    'defaults' => ['slug' => '/'],
                ],
                [
                    'pattern' => '<slug>/<uri>',
                    'route' => 'site/slug',
                    'defaults' => ['slug' => '/', 'uri' => false],
                ],
                //'artykuly' => 'articles/index',
                ///'artykul/<id>/' => 'articles/view',
                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    /*
      'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'rules' => [
      ],
      ],
     */
    ],
    'params' => $params,
];
